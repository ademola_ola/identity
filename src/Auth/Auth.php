<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/5/16
 * Time: 11:46 AM
 */
namespace App\Auth;
use App\Models\User;
use App\Models\UserDetail;
class Auth
{

    public function check() {
        if (isset($_SESSION['user'])){
            $user = User::where('id', $_SESSION['user'])->first();
            if ($user) {
                return true;
            }
            unset($_SESSION['user']);
        };
        return false;
    }

    public function user() {
        $user = User::where('id', $_SESSION['user'])->first();
        return UserDetail::where('phone', $user->phone)->first();
    }                                                                                                           

    public function attempt($phone, $password) {

        $user = User::where('phone', $phone)->first();

        if (!$user) {
            return false;
        }

        $check = openssl_digest($password, 'sha512');

        if ($check == $user->password) {
            $_SESSION['user'] = $user->id;
            return true;
        }
        return false;
    }

    public function logout() {
        unset($_SESSION['user']);
    }

}