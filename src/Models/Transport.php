<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/12/16
 * Time: 3:21 PM
 */

namespace App\Models;


class Transport
{

    protected $table = 'transports';

    protected $fillable = [
        "domain",
        "transport",
        "gid"
    ];
}