<?php
/**
 * Created by PhpStorm.
 * User: codesmata
 * Date: 1/19/17
 * Time: 11:12 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{

    protected $table="institutions";
    protected $fillable=['user_id','school_name','course','certificate','grad_year'];
}