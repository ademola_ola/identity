<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 9/22/16
 * Time: 10:50 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    protected $table = 'reminders';

    protected $fillable = [
        "name",
        "date",
        "time",
        "is_repeated"
    ];

}