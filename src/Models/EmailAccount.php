<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/12/16
 * Time: 1:18 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailAccount extends Model
{
    protected $table = 'email_accounts';

    protected $fillable = [
        "email",
        "mail_server",
        "access_token",
        "phone"
    ];

    public function setAccessToken($token) {
        $this->update([
            'access_token' => $token
        ]);
    }
}