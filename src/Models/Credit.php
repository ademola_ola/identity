<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 10/28/16
 * Time: 2:59 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    protected $table = 'credits';

    protected $fillable = [
        "units",
        "phone"
    ];

    public function withdraw($current, $value=1) {

        if($value > $current) {
            $result = 0;
        }
        else {
            $result = $current - $value;
        }
        $this->update([
            'units' => $result
        ]);
    }
    
    public function deposit($current, $value=1)
    {
        $this->update([
            'units' => $current + $value
        ]);
    }

}