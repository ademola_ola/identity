<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/5/16
 * Time: 1:21 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $table = 'user_details';

    protected $fillable = [
        "website_url", "email", "password", "plain_password", "uid", "gid", "phone", "maildir", "user_id", "first_name", "last_name",
        "realname", "address", "country", "institution", "graduation_date", "company_name", "work_period",
        "web_notification", "push_notification", "mobile_notification", "id"
    ];

    public function updateProfile($first_name, $last_name, $address, $country, $institution, $graduation_date, $company_name,
                                  $work_period, $web_notification, $push_notification, $mobile_notification) {
        $this->update([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'realname' => $first_name.' '.$last_name,
            'address' => $address,
            'country' => $country,
            'institution' => $institution,
            'graduation_date' => $graduation_date,
            'company_name' => $company_name,
            'work_period' => $work_period,
            'web_notification' => $web_notification,
            'push_notification' => $push_notification,
            'mobile_notification' => $mobile_notification
        ]);
    }

    public function setPassword($password) {
        $this->update([
            'password' => openssl_digest($password, 'sha512'),
            'plain_password' => $password
        ]);
    }
}