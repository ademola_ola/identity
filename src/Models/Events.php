<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 9/22/16
 * Time: 10:49 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $table = 'events';

    protected $fillable = [
        "name",
        "description",
        "start_date",
        "end_date",
        "link",
        "phone",
        "uniqueid"
    ];
}