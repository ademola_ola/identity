<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/5/16
 * Time: 10:10 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class VirtualDomain extends Model
{
    protected $table = 'virtual_domains';

    protected $fillable = [
        "name"
    ];
}