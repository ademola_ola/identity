<?php
/**
 * Created by PhpStorm.
 * User: codesmata
 * Date: 1/20/17
 * Time: 10:01 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserPreference extends Model
{

    protected $table="user_preferences";
    protected $fillable=['user_id','signature','undo_timer'];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

}