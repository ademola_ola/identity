<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 9/22/16
 * Time: 10:48 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
        "user_phone",
        "first_name",
        "other_names",
        "full_name",
        "email_address",
        "phone",
        "image_url",
        "is_active"
    ];

    public function owner(){
        return $this->belongsTo('App\Models\User','user_phone','phone');
    }
}

