<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 10/28/16
 * Time: 10:52 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $table = 'messages';

    protected $fillable = [
        "text",
        "phone",
        "recipient"
    ];

}