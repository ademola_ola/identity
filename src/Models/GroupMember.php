<?php
/**
 * Created by PhpStorm.
 * User: codesmata
 * Date: 1/17/17
 * Time: 11:37 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    protected $table="group_members";

    protected $fillable=['user_id','group_id','contact_id'];

    function group(){
        return $this->belongsTo('App\Models\Group','group_id');
    }

    function contact(){
        return $this->belongsTo('App\Model\Contacts','contact_id','id');
    }
}