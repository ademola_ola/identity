<?php
/**
 * Created by PhpStorm.
 * User: codesmata
 * Date: 1/19/17
 * Time: 12:11 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class WorkPlace extends Model
{

    protected $table="work_places";
    protected $fillable=['user_id','workplace','post_held','start_date','end_date','job_desc'];

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

}