<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 10/28/16
 * Time: 11:46 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Feeds extends Model
{
    protected $table = 'feeds';

    protected $fillable = [
        "text",
        "phone",
        "creator"
//        "is_public",
//        "is_private"
    ];
}