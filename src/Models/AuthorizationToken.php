<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/26/16
 * Time: 12:35 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AuthorizationToken extends Model
{
    protected $table = 'authorization_tokens';

    protected $fillable = [
        "phone",
        "is_expired",
        "access_token"
    ];

    public function expire() {
        $this->update([
            'is_expired' => true
        ]);
    }
}