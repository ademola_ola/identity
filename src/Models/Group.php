<?php
/**
 * Created by PhpStorm.
 * User: codesmata
 * Date: 1/17/17
 * Time: 11:35 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

    protected $table="groups";

    protected $fillable=['user_id','group_name'];

    function members(){
        return $this->hasMany('App\Models\GroupMember','group_id','id');
    }
}