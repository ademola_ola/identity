<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 1:20 PM
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Contacts;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = [
        "phone",
        "password",
        "plain_password"
    ];

    public function setPassword($password) {
        $this->update([
           'password' => openssl_digest($password, 'sha512')
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * gets the contacts information for this user
     */
    function contacts(){
        return $this->hasMany('App\Models\Contacts','user_phone','phone');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * gets the detail of the user
     */
    function userDetail(){
        return $this->hasOne('App\Models\UserDetail','phone','phone');
    }

    function groups(){
        return $this->hasMany('App\Models\Group','user_id','id');
    }

    function educations(){
        return $this->hasMany('App\Models\Institution','user_id','id');
    }

    function workplaces(){
        return $this->hasMany('App\Models\WorkPlace','user_id','id');
    }

    function preference(){
        return $this->hasOne('App\Models\UserPreference','user_id','id');
    }

}