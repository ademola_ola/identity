<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 9/22/16
 * Time: 10:49 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    protected $table = 'notes';

    protected $fillable = [
        "name",
        "text"
    ];
}