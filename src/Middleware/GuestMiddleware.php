<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/12/16
 * Time: 2:41 PM
 */

namespace App\Middleware;


class GuestMiddleware extends Middleware
{

    public function __invoke($request, $response, $next)
    {
        // TODO: Implement __invoke() method.
        if($this->container->auth->check()) {
            return $response->withRedirect($this->container->router->pathFor('landing'));
        }
        
        return $next($request, $response);
    }
}