<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 5:51 PM
 */

namespace App\Middleware;


class ValidationErrorsMiddleware extends Middleware
{

    public function __invoke($request, $response, $next) {

        if (isset($_SESSION['errors'])) {
            $this->container->view->getEnvironment()->addGlobal('errors', $_SESSION['errors']);
        }

        unset($_SESSION['errors']);

        $response = $next($request, $response);

        return $response;
    }
}