<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 10/7/16
 * Time: 12:44 AM
 */
namespace App\Controllers\Auth;

use App\Controllers\BaseController;
use App\Services\Utils;
use App\Services\Token;
use App\Models\User;
use App\Services\Messaging;

class ChangeController extends BaseController
{

    public function getPage($request, $response) {
        return $this->view->render($response, 'templates/auth/change.twig', [
            'countries' => Utils::load_codes()
        ]);
    }

    public function postPage($request, $response) {

        // insert logic to send out reset action here
        $device = (string)$request->getParam('countryCode')."". (string)(int)$request->getParam('phone');

        $user = User::where('phone', $device)->first();

        if (!$user) {
            $error = 'An account with this phone number was not found';
            return $this->view->render($response, 'templates/auth/change.twig', [
                'countries' => Utils::load_codes(),
                'error' => $error
            ]);
        }

        $token = Token::generateToken($device);

        Messaging::send_verification_sms($device, $token);

        return $response->withRedirect($this->router->pathFor('login'));
    }
}