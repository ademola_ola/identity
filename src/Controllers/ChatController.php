<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 9/20/16
 * Time: 1:32 AM
 */
namespace App\Controllers;
use App\Models\EmailAccount;

class ChatController
{

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function getPage($request, $response){

        $user = $this->auth->user();

        $email_accounts = array($user->email);

        $accounts = EmailAccount::where('phone', $user->phone)->get();

        foreach ($accounts as $acc) {
            array_push($email_accounts, $acc->email);
        }

        return $this->view->render($response, 'templates/admin/chat.twig', [
            'user' => $user,
            'email_accounts' => $email_accounts,
            'page_title' => 'Compose Message'
        ]);
    }

    /**
     * @param $request
     * @param $response
     */
    public function postData($request, $response) {

    }
}
