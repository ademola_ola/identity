<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 10/6/16
 * Time: 3:39 PM
 */
namespace App\Controllers;
use App\Controllers\BaseController;
use App\Models\EmailAccount;

class YahooController extends BaseController
{
    public function postData($request, $response){

        $user = $this->auth->user();

        $data = $request->getParam('email');

        $email = $data.'@yahoo.com';

        EmailAccount::create([
            "email" => $email,
            "mail_server" => "yahoo",
            "access_token" => "",
            "phone" => $user->phone
        ]);
        

        return $response->withRedirect($this->router->pathFor('landing'));
    }
}