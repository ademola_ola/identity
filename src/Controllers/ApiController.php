<?php

/**
 * Created by PhpStorm.
 * User: stikks-workstation
 * Date: 11/22/16
 * Time: 2:40 PM
 */
namespace App\Controllers;
use App\Models\Contacts;
use App\Models\User;
use App\Services\CodeUtility;

class ApiController extends BaseController
{

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function getContacts($request, $response, $args) {

        $user_id = null;

        if (isset($args['user_id'])) {
            $user_id = $args['user_id'];
            if(!$user_id) {
                return $response->withStatus(400)->write('Missing user ID');
            }
        }

        else {
            return $response->withStatus(400)->write('Missing user ID');
        }

        $user = User::where('id', (integer)$user_id)->first();
        $contacts = Contacts::where('user_phone', $user->phone)->get();

        $con_info=[];

        foreach ($contacts as $contact){
            $con_info[$contact->id]=$contact->full_name;
        }
        $con_info[]=['more'=>false];


        return $response->withJson($contacts);
//        return $response->withJson(['results'=>$con_info]);
    }

    public function getPlatformContacts($request,$response){
        $req_params=$request->getParams();
        $value=$req_params['q'];

        $contacts=[];

        $db=$this->container->db->connection()->getPdo();
        $contact_details=$db->query("SELECT id,first_name,last_name,phone,email FROM user_details WHERE first_name LIKE '%$value%'
 or last_name LIKE '%$value%' OR phone LIKE '%$value%' or email LIKE '%$value%' ")->fetchAll($db::FETCH_ASSOC);

        if(count($contact_details)>0){
            CodeUtility::log_info(json_encode($contact_details));

            foreach ($contact_details as $contact_detail){
                $contacts[]=$contact_detail;
            }
        }

        return CodeUtility::returnMessage(200,'success','Contacts Loaded',$contacts);
    }
}