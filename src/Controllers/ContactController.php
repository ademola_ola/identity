<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 11/1/16
 * Time: 10:37 AM
 */
namespace App\Controllers;
use App\Models\Contacts;
use App\Models\EmailAccount;
use App\Models\Group;
use App\Models\GroupMember;
use App\Services\CodeUtility;
use App\Services\ContactManagement;
use App\Services\GroupManagement;
use Google_Client;
use Google_Service_Calendar;
use Predis\Client;
include "Helper.php";
define('SCOPES', implode(' ', array(
        Google_Service_Calendar::CALENDAR_READONLY)
));

define('CLIENT_SECRET_PATH', __DIR__ . '/../../clients.json');

class ContactController extends BaseController
{

    public function getPage($request, $response) {

        $user = $this->auth->user();

        $contacts = Contacts::where('user_phone', $user->phone)->get();

        $link = $request->getUri()->getBaseUrl();

        $email_accounts = array();
        $accounts = EmailAccount::where('phone', $user->phone)->get();
        
        $default_sender = null;

        foreach ($accounts as $acc) {
            if ($acc->default_sender) {
                $default_sender = $acc->email;
            }

            array_push($email_accounts, $acc);
        }
        
        return $this->view->render($response, 'templates/admin/contacts.twig', [
            'user' => $user,
            'contacts' => $contacts,
            'page_title' => 'Compose Message',
            'link' => $link,
            'default_sender' => $default_sender,
            'accounts' => $email_accounts
        ]);
    }


    public function syncGoogle($request, $response) {

        $client = new Google_Client();
        $client -> setApplicationName('1Identiti');
        $client->addScope('https://www.google.com/m8/feeds');

        $client -> setClientId('750074493137-sskup8gsc9g3hi95od91e8s7aot2m9m9.apps.googleusercontent.com');
        $client -> setClientSecret("v-ESmPFRFPD6wCq6JqUrwCKG");
        $client -> setRedirectUri('http://localhost:9089/contacts/google_feedback');
//        $client -> setRedirectUri('https://unodentity.com/contacts/google_feedback');
        $client -> setAccessType('offline');

        $authUrl = $client->createAuthUrl();
        return file_get_contents($authUrl);
    }


    public function redirectRequest($request, $response) {

        $client = new Google_Client();
        $client->setApplicationName('1Identiti');
        $client->addScope('https://www.google.com/m8/feeds');
        $client -> setClientId('750074493137-sskup8gsc9g3hi95od91e8s7aot2m9m9.apps.googleusercontent.com');
        $client -> setClientSecret("v-ESmPFRFPD6wCq6JqUrwCKG");
        $client -> setRedirectUri('http://localhost:9089/contacts/google_feedback');
//        $client -> setRedirectUri('https://unodentity.com/contacts/google_feedback');
        $client->setAccessType('offline');

        $code = $request->getParam('code');
        $resp = $client->fetchAccessTokenWithAuthCode($code);

        if ($resp['error']){
            return $response->withRedirect($this->router->pathFor('landing'));
        }

        $accessToken = $resp['access_token'];

        $url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results=200&alt=json&v=3.0&oauth_token='.$accessToken;

        $xml_response = HTTP(0, $url, array("Authorization: Bearer $accessToken"), 0);

        $user = $this->auth->user();

        foreach ($xml_response['feed']['entry'] as $entry) {

            $phone = str_replace(str_split('tel:-'), '', $entry['gd$phoneNumber'][0]['uri']);

            $email = $entry['gd$email'][0]['address'];

            $exists = false;
            $active = false;

            if ($phone) {
                // check if contact is registered
                $match = array('user_phone' => $user->phone, 'phone' => $phone);
                $exists = Contacts::where($match)->exists();

                // check if account exists
                $account = EmailAccount::where('phone', $phone)->first();
                if ($account) {
                    $active = true;
                }
            }

            if (!$exists) {

                $image_url = null;

                if (isset($entry['link'][0]['href'])) {

                    $url =   $entry['link'][0]['href'];

                    $url = $url . '&access_token=' . urlencode($accessToken);

                    $curl = curl_init($url);

                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curl, CURLOPT_TIMEOUT, 15);
                    curl_setopt($curl, CURLOPT_VERBOSE, true);

                    $image_url = curl_exec($curl);
                    curl_close($curl);
                }

                $data = array(
                    'full_name' => $entry['title']['$t'],
                    'email_address' => $email,
                    'user_phone' => $user->phone,
                    'image_url' => $image_url,
                    'is_active' => $active,
                    'phone' => $phone
                );

                Contacts::create($data);

            }
        }

        return $response->withRedirect($this->router->pathFor('contacts'));
    }

    /**
     * @param $request
     * @param $response
     */
    public function syncYahoo($request, $response) {
        var_dump($request->getParams());
        exit();
    }


    /**
     * @param $request
     * @param $response
     *
     * Creates group
     */
    public function createGroup($request,$response){
        $group_name=$request->getParam('group_name');
        //$userid=1;
        $userid=$_SESSION['user'];
        //$userid="Hello";

        if($group_name=="" || $group_name==null){
            echo CodeUtility::returnMessage(405,'error','Missing Field','Please provide a valid group name');
        }

        $groupManager=new GroupManagement();
        echo $groupManager->createNewGroup($userid,$group_name);
    }

    /**
     * Loads the group available for a particular user
     */
    function loadUserGroups($request, $response){

        //$user = $this->auth->user();
        $user=1;
        $groupManager = new GroupManagement();
        echo $groupManager->listUserGroup($user);
//        echo $groupManager->listUserGroup($user->id);
    }


    /**
     * @param $request
     * @param $response
     *
     * Creates Individual Contact
     */
    function createContact($request,$response){
        $group_id=$request->getParam('contact_group');
        $firstname=$request->getParam('contact_first_name');
        $lastname=$request->getParam('contact_last_name');
        $email=$request->getParam('contact_email');
        $phone=$request->getParam('contact_phone');

        $contact_info=[
            'group_id'=>$group_id,
            'firstname'=>$firstname,
            'lastname'=>$lastname,
            'email'=>$email,
            'phone'=>$phone
        ];

        //send this parameters to the ContactManager
        $contactManager=new ContactManagement();
        echo $contactManager->createNewContact(json_encode($contact_info));
    }

    /**
     * @param $request
     * @param $response
     *
     * This handles the upload of contact file
     */
    function uploadContact($request,$response){
        $file=$_FILES['upload_contact_file'];

        if(empty($file)){
            echo CodeUtility::returnMessage(405,'error','Invalid File','Please Upload a valid file!');
        }

        if($file['type']!=='application/vnd.ms-excel'){
            echo CodeUtility::returnMessage(405,'error','Invalid File format','This is an invalid file format, please upload excel format');
            return;
        }

        $destination_path='uploads/';
        if(!file_exists($destination_path)){
            mkdir($destination_path,0777);
        }
        $destination_file=$destination_path.'/'.time().'_'.$file['name'];

        if(!move_uploaded_file($file['tmp_name'],$destination_file)){
          echo CodeUtility::returnMessage(405,'error','Error Uplaoding Contact List','An error occurred while uplaoding the contact list!');
        }

        echo $this->readFileToDb($destination_file);
    }

    /**
     * @param $file
     * @return string
     *
     * this reads the uploaded files to the database
     */
    function readFileToDb($file){
        try{
            $excelObject=\PHPExcel_IOFactory::load($file);
            $excelWorksheet=$excelObject->getActiveSheet();


//            $highestColumn=$excelWorksheet->getHighestColumn();
            $highestRow=$excelWorksheet->getHighestRow();

            for ($i=2;$i<=$highestRow; $i++){
                $firstName=$excelWorksheet->getCellByColumnAndRow(0,$i)->getValue();
                $lastName=$excelWorksheet->getCellByColumnAndRow(1,$i)->getValue();
                $email=$excelWorksheet->getCellByColumnAndRow(2,$i)->getValue();
                $phone=$excelWorksheet->getCellByColumnAndRow(3,$i)->getValue();

                $contact_info=[
                    'group_id'=>0,
                    'firstname'=>$firstName,
                    'lastname'=>$lastName,
                    'email'=>$email,
                    'phone'=>$phone
                ];

                $contactManager=new ContactManagement();
                $contactManager->createNewContact(json_encode($contact_info));
            }
        }
        catch(Exception $ex){
            return CodeUtility::returnMessage(405,'error','Error occurred',$ex->getMessage());
        }

        return CodeUtility::returnMessage(200,'success',"Contacts Added Successfully!! You May want to add them to groups","");
    }

    function keepContactGroup($request,$response){

        $user_id=$this->auth->user()->id;
        $group_id=$request->getParam('add_group_name');
        $contacts=$request->getParam('add_contact_list');
        $group_info=Group::find($group_id);

        $manager=new ContactManagement();

        if(count($group_info)<=0){
            return CodeUtility::returnMessage(405,'error','Missing Group','Seens this group has been removed... Please reload the page');
        }


        foreach ($contacts as $contact){
            //check if this contact exist for this group
            if($manager->keepUserInGroup($contact,$group_id,$user_id)!=='success'){
                CodeUtility::log_info("An error occurred while Keeping This contacts informtion");
            }

        }                                       //end of the loop

        return CodeUtility::returnMessage(200,'success','Contacts Added to the group','');
    }

    function loadGroupContacts($request,$response){
        $group_members=[];
        $route=$request->getAttribute('route');
        $group_id=$route->getArgument('group_id');

        $group_info=Group::find($group_id);

        if(count($group_info)>0){
            //lets get the group Members
            $members=$group_info->members;

            if(count($members)>0){
                foreach ($members as $member){
                    $member_info=Contacts::find($member->contact_id);
                    $group_members[]=$member_info;
                }
            }
        }

        //return response()->getBody()->withJson($group_members);
        print_r($group_members);
    }

    public function getGroups($request, $response) {

        $user = $this->auth->user();

        $groups = Group::where('user_id', $user->id)->get();

        return $this->view->render($response, 'templates/admin/groups.twig', [
            'user' => $user,
            'groups' => $groups,
            'page_title' => 'Groups'
        ]);
    }

    public function getGroup($request, $response, $args) {

        $user = $this->auth->user();

        $id = $args['group_id'];

        $group = Group::find($id);

        return $this->view->render($response, 'templates/admin/group.twig', [
            'user' => $user,
            'group' => $group,
            'page_title' => 'Group'
        ]);
    }
}

