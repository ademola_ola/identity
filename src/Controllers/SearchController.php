<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 9/20/16
 * Time: 6:02 PM
 */
namespace App\Controllers;
use App\Services\Index;
use App\Services\Utils;
use App\Services\CodeUtility;

class SearchController extends BaseController
{
    
    public function getPage($request, $response){

        $value = $request->getParam('q');

        if (!$value) {
            $value = "";
        }

//        else if (substr($param, 0, 1) === '+') {
//            $param = substr($param, 1);
//        }
//
//        $index = new Index();
//
//        $result = $index->search_object('user_detail', 'phone', $param);
        
        $user = $this->auth->user();

        $contacts = [];

        $db=$this->container->db->connection()->getPdo();
        $contact_details=$db->query("SELECT id,first_name,last_name,phone,email FROM user_details WHERE first_name LIKE '%$value%'
 or last_name LIKE '%$value%' OR phone LIKE '%$value%' or email LIKE '%$value%' ")->fetchAll($db::FETCH_ASSOC);

        if(count($contact_details)>0){
            CodeUtility::log_info(json_encode($contact_details));

            foreach ($contact_details as $contact_detail){
                array_push($contacts, $contact_detail);
            }
        }

        return $this->view->render($response, 'templates/admin/results.twig', [
            'user' => $user,
            'countries' => json_encode(Utils::load_codes()),
            'accounts' => $contacts,
            'total' => count($contacts),
            'q' => $value
        ]);
    }

}