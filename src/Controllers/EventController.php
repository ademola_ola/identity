<?php

/**
 * Created by PhpStorm.
 * User: stikks-workstation
 * Date: 12/5/16
 * Time: 12:11 PM
 */
namespace App\Controllers;
use App\Models\Events;

class EventController extends BaseController
{
    public function getPage($request, $response){

        $user = $this->auth->user();

        $events = Events::where("phone", $user->phone)->get();

        return $this->view->render($response, 'templates/events.twig', [
            'user' => $user,
            'page_title' => 'Home',
            'events' => $events
        ]);
    }


    function createEvent(){
        echo 'You want to create event';
    }

}