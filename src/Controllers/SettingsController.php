<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/12/16
 * Time: 5:41 PM
 */

namespace App\Controllers;
use App\Auth\Auth;
use App\Models\User;
use App\Models\UserPreference;
use App\Models\WorkPlace;
use App\Services\CodeUtility;
use Respect\Validation\Validator as Val;
use App\Models\Institution;
use App\Services\Utils;

class SettingsController extends BaseController
{


//    function __construct($container)
//    {
//        $this->c=$container;
//        //$this->auth=Aut
//    }

    public function getPage($request, $response){

        $user = $this->auth->user();
        //$user=$_SESSION['user'];

        return $this->view->render($response, 'templates/settings.twig', [
            "user" => $user,
            'countries' => Utils::load_codes()
        ]);
    }


    /**
     * @param $request
     * @param $response
     */
    public function createBasicSetting($request,$response){
        $user_id=$_SESSION['user'];
        $alias=$request->getParam('alias');
        $first_name=$request->getParam('first_name');
        $last_name=$request->getParam('last_name');
        $address=$request->getParam('address');
        $country=$request->getParam('country');

        $user_info=User::find($user_id);
        if(count($user_info)<=0){
            echo CodeUtility::returnMessage(405,'error','User Not Logged In','Please Login to update your information!!');
            return;
        }

        $user_details=$user_info->userDetail;

        //lets update those datas
        $user_details->realname=$alias;
        $user_details->first_name=$first_name;
        $user_details->last_name=$last_name;
        $user_details->address=$address;
        $user_details->country=$country;

        $user_info_upd=$user_details->save();

        if(!$user_info){
            echo CodeUtility::returnMessage(405,'error','User Not Logged In','Please Login to update your information!!');
            return;
        }

        echo CodeUtility::returnMessage(200,'success','Basic Info Updated Successfully!','Your Basic Information has been updated successfully!!');
    }


    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function createEducationSetting($request,$response){
        $institutions=$request->getParam('institution');
        $courses=$request->getParam('course');
        $certificates=$request->getParam('certificate');
        $grad_dates=$request->getParam('graduation_date');
        $user_id=$_SESSION['user'];

        $db=$this->container->db->connection()->getPdo();

        //lets process the education information
        $i=0;

        $db->beginTransaction();

        try{
            foreach ($institutions as $institution){
                $course=$courses[$i];
                $certificate=$certificates[$i];
                $grad_date=$grad_dates[$i];


                if($institution!=="" && $course!=="" && $certificate!=="" && $grad_date!==""){
                    //check if this user has registered this school and grade before
                    $school_check=Institution::whereRaw('user_id=? and school_name=? and course=? and certificate=? and grad_year=?',
                        [$user_id,$institution,$course,$certificate,$grad_date])->get();

                    if(count($school_check)<=0){
                        //register a new school for the user if it hasnt been registered for the user already
                        $school_create=Institution::create([
                            'user_id'=>$user_id,
                            'school_name'=>$institution,
                            'course'=>$course,
                            'certificate'=>$certificate,
                            'grad_year'=>$grad_date
                        ]);

                        if(!$school_create){
                            return CodeUtility::errorData($response,500,'error','Error In connection','An error occurred while connecting to the database');
                        }
                    }
                    else{
                        CodeUtility::log_info("This User Already Registered this school record");
                    }
                }else{
                    CodeUtility::log_info("Some Fields are empty so we ignore");
                }

                $i++;
            }

        }
        catch(Exception $ex){
            $db->rollBack();
            return CodeUtility::errorData($response,500,'error','Error In connection',$ex->getMessage());
        }

        $db->commit();
        return CodeUtility::errorData($response,200,'success','Education Record Successfully Created','');
    }


    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function loadEducationSettings($request,$response){
        $user_id=$this->auth->user()->id;

        if($user_id==null || $user_id==""){
            //ask the user to login before they can view their profile page
        }

        $user_info=User::find($user_id);

        $educations=[];
        if($user_info->educations!==null){
            $educations=$user_info->educations;
        }

        return CodeUtility::errorData($response,200,'success','User Education Information Found',$educations);
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function createWorkExperience($request,$response){
        $companies=$request->getParam('company_name');
        $post_helds=$request->getParam('post_held');
        $job_descs=$request->getParam('job_description');
        $start_dates=$request->getParam('start_date');
        $end_dates=$request->getParam('end_date');
        $user_id=$_SESSION['user'];

        $db=$this->container->db->connection()->getPdo();

        $i=0;

        $db->beginTransaction();
        try{

            foreach ($companies as $company){
                $post_held=$post_helds[$i];
                $job_desc=$job_descs[$i];
                $start_date=$start_dates[$i];
                $end_date=$end_dates[$i];

                if($post_held !=="" && $job_desc!=="" && $start_date!=="" && $end_date!==""){

                    //check if this experience has been registered for this user
                    $post_check=WorkPlace::whereRaw('user_id=? and post_held=? and workplace=?',[$user_id,$post_held,$company])->get();

                    if(count($post_check)<=0){
                        //we create the record here
                        $post_crt=WorkPlace::create([
                            'user_id'=>$user_id,
                            'workplace'=>$company,
                            'post_held'=>$post_held,
                            'start_date'=>$start_date,
                            'end_date'=>$end_date,
                            'job_desc'=>$job_desc
                        ]);

                        if(!$post_crt){
                            return CodeUtility::errorData($response,500,'error','Error In Connection','An error occurred while connecting to the database');
                        }
                    }else{
                        CodeUtility::log_info("This Position in this company has been registered already");
                    }
                }else{
                    CodeUtility::log_info("Some Fields Were not filled so they were ignored");
                }

                $i++;
            }
        }
        catch (Exception $ex){
            $db->rollBack();
            return CodeUtility::errorData($response,500,'error','Error Occurred',$ex->getMessage());
        }

        $db->commit();
        return CodeUtility::errorData($response,200,'success','Successful','Work Experience Updated Successfully!!');
    }

    public function loadWorkExperiences($request, $response){

        $user_id=$this->auth->user()->id;

        if($user_id==null || $user_id==""){
            //ask the user to login before they can view their profile page
        }

        $user_info=User::find($user_id);
        $work_places=[];

        if($user_info->workplaces!==null){
            $work_places=$user_info->workplaces;
        }

       return CodeUtility::errorData($response,200,'success','Work Places Loaded',$work_places);
    }


    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    function createUserPreference($request,$response){

//        $signature=htmlspecialchars($request->getParam('signature'));
        $signature = $request->getParam('signature');

        $undoTimer = htmlspecialchars($request->getParam('undo_timer')) || 0;

        $user_id=$this->auth->user()->id;

        if($signature!==''){
            $user_info=User::find($user_id);

            if($user_info==null){
                return CodeUtility::errorData($response,405,'error','Invalid User','Please login to update your personal preference');
            }

            if(count($user_info->preference)>0){
                //exists so we update
                $user_info->preference->signature=$signature;
                $user_info->preference->undo_timer=$undoTimer;

                $user_upd=$user_info->preference->save();

                if(!$user_upd){
                    return CodeUtility::errorData($response,405,'error','Invalid User','An error occurred while ');
                }
            }
            else{
                //doesnt exist so we create
                $user_pre_crt=UserPreference::create([
                    'user_id'=>$user_id,
                    'signature'=>$signature,
                    'undo_timer'=>$undoTimer
                ]);

                if(!$user_pre_crt){
                    return CodeUtility::errorData($response,405,'error','Invalid User','An error occurred while creating your user preferebce');
                }
            }
        }
        return CodeUtility::errorData($response,200,'success','Personal Preference Updated Successfully!!!','');
    }


    public function loadUserPreference($request,$response){
        $user_id=$this->auth->user()->id;
        $user_info=User::find($user_id);

        if($user_info==null){
            return CodeUtility::errorData($response,405,'error','Invalid User','Please login to update your personal preference');
        }

        return CodeUtility::errorData($response,200,'success','User Preference Loaded Successfully',$user_info->preference);
    }


    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function postData($request, $response) {

        $user = $this->auth->user();

        $user->updateProfile(
            $request->getParam('first_name'),
            $request->getParam('last_name'),
            $request->getParam('address'),
            $request->getParam('country'),
            $request->getParam('institution'),
            $request->getParam('graduation_date'),
            $request->getParam('company_name'),
            $request->getParam('work_period'),
            $request->getParam('web_notification'),
            $request->getParam('push_notification'),
            $request->getParam('mobile_notification')
        );
        
        return $this->view->render($response, 'templates/settings.twig', [
            "user" => $user,
            'countries' => Utils::load_codes()
        ]);

    }
}