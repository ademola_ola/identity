<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/12/16
 * Time: 5:41 PM
*/

namespace App\Controllers;
use App\Services\Utils;
use App\Services\Index;
use App\Models\UserDetail;
use App\Models\WorkPlace;
use App\Models\Institution;
use App\Services\CodeUtility;


class SearchProfileController extends BaseController
{
    
    public function getPage($request, $response, $args){

        $user = $this->auth->user();

        $value = $args['number'];

        if (substr($value, 0, 1) != '+') {
            $value = '+'.$value;
        }

        $account = UserDetail::where('phone', preg_replace('/\s+/', '', $value))->first();

        $places = WorkPlace::where('user_id', $account->id)->get();

        $institutions = Institution::where('user_id', $account->id)->get();

        return $this->view->render($response, 'templates/user.twig', [
            'user' => $user,
            'countries' => Utils::load_codes(),
            'account' => $account,
            'work_places' => $places,
            'institutions' => $institutions
        ]);
    }

    public function searchResult($request, $response, $args) {

        $value = $request->getParam('q');

        if (!$value) {
            return $response->withRedirect($this->router->pathFor('search'));
        }

        $user = $this->auth->user();
        $contacts = [];

        $db=$this->container->db->connection()->getPdo();
        $contact_details=$db->query("SELECT id,first_name,last_name,phone,email FROM user_details WHERE first_name LIKE '%$value%'
 or last_name LIKE '%$value%' OR phone LIKE '%$value%' or email LIKE '%$value%' ")->fetchAll($db::FETCH_ASSOC);

        if(count($contact_details)>0){
            CodeUtility::log_info(json_encode($contact_details));

            foreach ($contact_details as $contact_detail){
                array_push($contacts, $contact_detail);
            }
        }

        return $this->view->render($response, "templates/admin/results.twig", [
            'user' => $user,
            'accounts' => $contacts,
            'q' => $value,
            'count' => count($contacts)
        ]);
    }

}