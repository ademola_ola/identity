<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 1:55 PM
 */

namespace App\Controllers;

use \Slim\Views\Twig as View;

class BaseController
{
    protected $container;

    public function __construct($container)
    {

        $this->container = $container;
    }

    public function __get($name)
    {
        if ($this->container->{$name}) {
            return $this->container->{$name};
        }
    }
}