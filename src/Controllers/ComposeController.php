<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 9/20/16
 * Time: 1:30 AM
 */
namespace App\Controllers;

error_reporting(E_ALL);
ini_set('display_errors', 1);

use App\Models\EmailAccount;
use App\Models\User;
use App\Services\CodeUtility;
use App\Services\Messaging;
use App\Services\Queueing;
use Zend\Mail\Storage\Imap;
use Zend\Mail\Storage;
use Zend\Mail\Storage\Folder;
use Zend\Mail\Message;
use App\Services\CodeUtility as util;
use App\Models\UserPreference;

class ComposeController extends BaseController
{

    public function getPage($request, $response, $args){

        $user = $this->auth->user();
        $default_sender = $user->email;
        $email_accounts = array();
        $accounts = EmailAccount::where('phone', $user->phone)->get();

        foreach ($accounts as $acc) {
            if ($acc->default_sender) {
                $default_sender = $acc->email;
            }
            array_push($email_accounts, $acc);
        }

        $preference = UserPreference::where('user_id', $user->id)->first();

        if (!$preference) {
            UserPreference::create([
                'user_id'=>$user->id,
                'signature'=>' ',
                'undo_timer'=>1
            ]);
        }

        return $this->view->render($response, 'templates/admin/compose.twig', [
            'user' => $user,
            'email_accounts' => $email_accounts,
            'page_title' => 'Compose Message',
            'default_sender' => $default_sender,
            'preference' => $preference
        ]);
    }

    public function sendContact($request, $response, $args) {

        $user = $this->auth->user();
        $default_sender = $user->email;
        $email_accounts = array();
        $accounts = EmailAccount::where('phone', $user->phone)->get();

        foreach ($accounts as $acc) {
            if ($acc->default_sender) {
                $default_sender = $acc->email;
            }

            array_push($email_accounts, $acc);
        }

        return $this->view->render($response, 'templates/admin/compose.twig', [
            'user' => $user,
            'email_accounts' => $email_accounts,
            'page_title' => 'Compose Message',
            'default_sender' => $default_sender,
        ]);

    }

    public function saveDraft($request, $response) {

        $user = $this->auth->user();

        $mail = new Imap([
            'host'     => 'mail.unodentity.com',
            'user'     => $user->email,
            'password' => $user->plain_password,
        ]);

        $mail->selectFolder('Drafts');

        $message = new Message();

        $message->addFrom($user->email, $user->phone);
        $to = explode(",", $request->getParam('to'));

        if(count($to)>0){
            foreach ($to as $t){
                if($t!==""){
                    $message->addTo($t);
                }
            }
        }

        $cc = explode(",", $request->getParam('cc'));
        if(count($cc)>0){
            foreach ($cc as $c){
                if(strlen($c) > 0){
                    $message->addBcc($c);
                }
            }
        }

        $bcc = explode(",", $request->getParam('bcc'));
        if(count($bcc)>0){
            foreach ($bcc as $bc){
                if(strlen($bc) > 0){
                    $message->addBcc($bc);
                }
            }
        }


        $subject = $request->getParam('subject');
        $message->setSubject($subject);

        $msg = $request->getParam('message');
        $message->setBody($msg);

        $mail->appendMessage($message->toString());

        return util::sendMessage($response, 200, 'success', 'Message saved to draft', array('response'=>'success'));
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     *
     * Queue the email for sending later
     */
    public function postData($request, $response)
    {

        $user = $this->auth->user();
        $from = htmlspecialchars($request->getParam('from'));
        $body = $request->getParam('message');
        $subject = htmlspecialchars($request->getParam('subject'));

        $to = explode(',',$request->getParam('to')[0]);
        $cc = explode(',', $request->getParam('cc')[0]);
        $bcc = explode(',', $request->getParam('bcc')[0]);

        $files = array();

        if(count($_FILES)>0){
            foreach ($_FILES as $file){

                for ($i=0; $i<count($file['name']); $i++){

                    $temp_file = './uploads/'. time(). '_' .$file["name"][$i];
                    move_uploaded_file($file['tmp_name'][$i], $temp_file);
                    array_push($files, array('name'=>$file["name"][$i], 'path'=>$temp_file));
                }

            }
        }

        $resp = Messaging::send_via_sendgrid($user->plain_password, $from, $subject, $body, $to, $cc, $bcc, $user->phone, $files);

        if ($resp) {
            return util::sendMessage($response, 200, 'success', 'Message sent', array('response'=>'success'));
        }
        return util::sendMessage($response, 200, 'error', 'Message not sent', array('response'=>'error'));

//        $queue = new Queueing();
//        if ($queue->queueMessage($msg)) {
//            return $response->withStatus(200)->write("success");
//        } else {
//            return $response->withStatus(500)->write("An error occurred while sending your mail... Please try again");
//        }

    }

    public function attachFiles($request, $response) {
        util::log_info($_FILES);
        exit();
    }

//    public function uploadFiles($request, $response) {
//
//        $files = array();
//
//        foreach ($_FILES as $file) {
//            $temp_file = '/opt/uploads/'. time(). '_' .$file["name"][0];
//            move_uploaded_file($file['tmp_name'][0], $temp_file);
//            array_push($files, array('name'=>$file["name"][0], 'path'=>$temp_file));
//        }
//    }

    /**
     * @param $oname
     * @param $newName
     *
     * @return String
     * This uploads the file
     */
    function uploadFile($oname,$newName){
        util::log_info(getcwd());
        if(!move_uploaded_file($oname,$newName)){
            return false;
        }else{
            return true;
        }
    }
}