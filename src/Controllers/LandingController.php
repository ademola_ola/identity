<?php
/**
 * Created by PhpStorm.
 * User: yusufbnahmad
 * Date: 15/08/16
 * Time: 11:34
 */

namespace App\Controllers;

include "Helper.php";
use App\Models\EmailAccount;
use App\Models\Events;
use Respect\Validation\Validator as Val;
use App\Services\Messaging;
use App\Models\Feeds;
use Predis\Client;

class LandingController extends BaseController
{

    public function getPage($request, $response){

        $user = $this->auth->user();

        $email_accounts = [];

        $accounts = EmailAccount::where('phone', $user->phone)->get();

        foreach ($accounts as $acc) {
            array_push($email_accounts, $acc);
        }

//        $feeds = Feeds::where('phone', $user->phone)->orderBy('updated_at', 'desc')->limit(2)->get();
//
//        $feed_count = count($feeds);
//
//        $events = Events::where("phone", $user->phone)->orderBy('start_date', 'asc')->limit(2)->get();

        return $this->view->render($response, 'templates/admin/index.twig', [
            'user' => $user,
            'email_accounts' => $email_accounts,
            'page_title' => 'Home'
//            'feeds' => $feeds,
//            'feed_count' => $feed_count,
//            'events' => $events
        ]);
    }
    
    public function postData($request, $response) {

        $user = $this->auth->user();

        $validation = $this->validator->validate($request, [
            'origin' => Val::notEmpty()->email(),
            'destination' => Val::notEmpty()->email(),
            'subject' => Val::notEmpty(),
            'message' => Val::notEmpty()
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor('landing'));
        }

        $name = $user->phone;

        if ($user->realname) {
            $name = $user->realname;
        }

        $match = ["email" => $request->getParam('origin'), 'phone' => $user->phone];

        $email = EmailAccount::where($match)->first();

        if ($email->mail_server == 'gmail') {
            $message = Messaging::send_via_gmail($email->access_token, $request->getParam('subject'), $request->getParam('destination'), $request->getParam('message'));
        }

        else {
            $message = Messaging::send_mail($request->getParam('subject'), $request->getParam('message'), $request->getParam('origin'), $request->getParam('destination'), $name);
        }
        
        if ($message != true) {
            $this->flash->addMessage('error', $message);
        }
        else{
            $this->flash->addMessage('basic', "Message successfully sent");
        }
        
        return $response->withRedirect($this->router->pathFor('landing'));
    }

    public function getFeed($request, $response){

        $user = $this->auth->user();
        
        $feeds = Feeds::where('phone', $user->phone)->orderBy('created_at', 'desc')->get();

        $feed_count = count($feeds);

        return $this->view->render($response, 'templates/admin/index.twig', [
            'user' => $user,
            'feeds' => $feeds,
            'feed_count' => $feed_count
        ]);
    }

    public function postFeed($request, $response) {

        $user = $this->auth->user();

        if ($user->realname) {
            $real_name = $user->realname;
        }
        else {
            $real_name = $user->phone;
        }

        try {
            $feed = Feeds::create([
                "text" => $request->getParam('message'),
                "phone" => $user->phone,
                "creator" => $real_name
            ]);

            if ($feed) {
                return $response->withStatus(200)->write('success');
            }
        }
        catch (\Exception $e) {
            return $response->withStatus(200)->write('error');
        }

        return $response->withStatus(200)->write('error');

    }

    public function streamNews($request, $response) {
        $url = 'http://voice.atp-sevas.com/demo/yql/news';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);

        $response = curl_exec($curl);
        $json = json_decode($response, true);
        curl_close($curl);

        $data = array_slice($json['data'], 0, 5);
        var_dump($data);
        exit();
    }
}