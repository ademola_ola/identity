<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 9/23/16
 * Time: 12:08 PM
 */
namespace App\Controllers;
use App\Controllers\BaseController;
use App\Models\EmailAccount;

class EmailController extends BaseController
{
    public function postData($request, $response){

        $user = $this->auth->user();

        return $response->withRedirect($this->router->pathFor('landing'));
    }
}