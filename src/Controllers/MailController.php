<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 1:43 PM
 */
namespace App\Controllers;
use App\Models\EmailAccount;
use App\Services\Messaging;
use Zend\Mail\Storage\Imap;
use Zend\Mail\Storage;
use Zend\Mail\Message;
use Zend\Mail\Storage\Folder;
use Predis\Client;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part;
use Zend\Mail\Transport;
use Zend\Mail\Protocol\Imap as ZendImap;
use RecursiveIteratorIterator;
use Respect\Validation\Exceptions\ZendException;

class MailController extends BaseController
{

    public function getMail($request, $response){

        $user = $this->auth->user();

        $email_accounts = [];
        
        $accounts = EmailAccount::where('phone', $user->phone)->get();

        foreach ($accounts as $acc) {
            array_push($email_accounts, $acc);
        }

//        $redis = new Client();
//
//        if(!$redis->hexists($user->email, "message_count")) {
//            $redis->hset($user->email, "message_count", 0);
//            $messageCount = 0;
//        }
//
//        else {
//            $messageCount = $redis->hget($user->email, 'message_count');
//        }

        return $this->view->render($response, 'templates/admin/mailbox.twig', [
            'user' => $user,
            'email_accounts' => $email_accounts,
            'page_title' => 'MailBox',
            'count' => 0
        ]);
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     *
     * retrieves sent emails
     */
    public function getSent($request, $response){

        $user = $this->auth->user();

//        $redis = new Client();
//
//        if(!$redis->hexists($user->email, "sent_count")) {
//            $redis->hset($user->email, "sent_count", 0);
//            $messageCount = 0;
//        }
//
//        else {
//            $messageCount = $redis->hget($user->email, 'sent_count');
//        }

        return $this->view->render($response, 'templates/admin/sent.twig', [
            'user' => $user,
            'page_title' => 'Sent Messages',
            'count' => 0
        ]);
    }


    /**
     * @param $request
     * @param $response
     * @return mixed
     *
     * retrieves drafts
     */
    public function getDrafts($request, $response){

        $user = $this->auth->user();

//        $redis = new Client();
//
//        if(!$redis->hexists($user->email, "drafts_count")) {
//            $redis->hset($user->email, "drafts_count", 0);
//            $messageCount = 0;
//        }
//
//        else {
//            $messageCount = $redis->hget($user->email, 'drafts_count');
//        }

        return $this->view->render($response, 'templates/admin/drafts.twig', [
            'user' => $user,
            'page_title' => 'Draft Messages',
            'count' => 0
        ]);
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     *
     * retrieves trasged messages
     */
    public function getTrash($request, $response){

        $user = $this->auth->user();

//        $redis = new Client();
//
//        if(!$redis->hexists($user->email, "trash_count")) {
//            $redis->hset($user->email, "trash_count", 0);
//            $messageCount = 0;
//        }
//
//        else {
//            $messageCount = $redis->hget($user->email, 'trash_count');
//        }

        return $this->view->render($response, 'templates/admin/trash.twig', [
            'user' => $user,
            'page_title' => 'Trash Messages',
            'count' => 0
        ]);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     *
     * This gets the detail of each message
     */

    public function getSpam($request, $response){

        $user = $this->auth->user();

        return $this->view->render($response, 'templates/admin/spam.twig', [
            'user' => $user,
            'page_title' => 'Spam Messages',
            'count' => 0
        ]);
    }

    /**
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     *
     * This gets the detail of each message
     */

    public function getDetail($request, $response, $args) {

        $user = $this->auth->user();

        $message_id = $args['message_id'];
        
        $folder = $args['folder'];

        if (!$message_id) {
            return $response->withRedirect($this->router->pathFor('mailbox'));
        }

//        try {

        $mail = new Imap([
            'host' => 'mail.unodentity.com',
            'user' => $user->email,
            'password' => $user->plain_password,
        ]);

        $mail->selectFolder($folder);
        $cc = '';
        $to = '';
        $reply_to = '';
        $from = '';
        $has_content = false;

        $message = $mail->getMessage($message_id);

        foreach ($message->getHeaders() as $head) {
            $field = $head->getFieldName();
            if ($field == 'Content-Type') {
                $has_content = true;
            }
            if ($field == 'cc') {
                $cc = $head->getFieldValue();
            }
            if ($field == 'to') {
                $to = $head->getFieldValue();
            }
            if ($field == 'reply_to') {
                $reply_to = $head->getFieldValue();
            }
            if ($field == 'From') {
                $from = $head->getFieldValue();
            }
        }

        $body =  $message->getContent();

        if ($has_content) {
            if ($message->isMultipart()) {
                $body = '';
                $part = null;
                foreach (new RecursiveIteratorIterator($message) as $part) {

                    //                if (strpos($part->contentType, 'text/plain') !== false) {
                    //                    $body = $body . utf8_encode($part->getContent()) . PHP_EOL;
                    //                }


                    if (strpos($part->contentType, 'text/html') !== false) {
                        $body = $body . quoted_printable_decode($part->getContent());
                    }
                }
            }

            else {
                if (strpos($message->contentType, 'text/plain') !== false) {
                    $body =  $message->getContent();
                }
                else if (strpos($message->contentType, 'text/html') !== false) {
                    $body =  quoted_printable_decode($message->getContent());
                }
            }
        }

        return $this->view->render($response, 'templates/admin/detail.twig', [
            'user' => $user,
            'page_title' => 'Message Details',
            'from' => $from,
            'to' => $to,
            'cc' => $cc,
            'reply_to' => $reply_to,
            'subject' => $message->subject,
            'body' => json_encode($body),
            'message_num' => $message_id,
            'folder' => $folder,
            'date' => $message->date
        ]);
//        }
//        catch (\Exception $e) {
//            return $response->withRedirect($this->router->pathFor('mailbox'));
//        }
    }

    public function trashMessage($request, $response, $args) {
        $user = $this->auth->user();
        $message_id = $args['message_id'];
        $folder = $args['folder'];

        try {
            $mail = new Imap([
                'host' => 'mail.unodentity.com',
                'user' => $user->email,
                'password' => $user->plain_password,
            ]);

            $mail->selectFolder($folder);

            $message = $mail->getMessage($message_id);

            $html = new Part($message->getContent());
            $html->type = "text/html";

            $bodyText = new MimeMessage();
            $bodyText->setParts(array($html));

            $new = new Message();
            $new->setBody($bodyText);

            $new->setFrom($message->from);
            $new->addTo($message->to);
            $new->setSubject($message->subject);

            Messaging::saveTrash($user->email, $user->plain_password, $new->toString());

            $mail->removeMessage($message_id);

            return $response->withJson(array('response'=>'success'));
        }
        catch (\Exception $e) {}

        return false;
    }

    public function deleteMessage($request, $response, $args) {
        $user = $this->auth->user();
        $message_id = $args['message_id'];
        $folder = $args['folder'];

        try {
            $mail = new Imap([
                'host' => 'mail.unodentity.com',
                'user' => $user->email,
                'password' => $user->plain_password,
            ]);

            $mail->selectFolder($folder);

            $mail->removeMessage($message_id);

            return $response->withJson(array('response'=>'success'));
        }
        catch (\Exception $e) {}

        return false;
    }

    public function syncMail($request, $response) {

        $user = $this->auth->user();

        try {
            $mail = new Imap([
                'host' => 'mail.unodentity.com',
                'user' => $user->email,
                'password' => $user->plain_password,
            ]);

            $messageCount = $mail->countMessages();

            $messages = [];

            foreach ($mail as $messageNum => $message) {
                $has_attachment = false;
                if (strpos($message->contentType, 'multipart/mixed') !== false) {
                    $has_attachment = true;
                }
                array_push($messages, array(
                        'from'=>$message->from,
                        "subject"=>$message->subject,
                        'date'=>$message->date,
                        'read'=>$message->hasFlag(Storage::FLAG_SEEN),
                        'number'=>$messageNum,
                        'has_attachment' => $has_attachment
                    )
                );
            }
        }

        catch (\Exception $e) {
            $messages = [];
            $messageCount = 0;
        }

        return json_encode(array('messages' => $messages, 'message_count' => $messageCount));
    }

    public function syncSent($request, $response) {

        $user = $this->auth->user();

        try {
            $mail = new Imap([
                'host'     => 'mail.unodentity.com',
                'user'     => $user->email,
                'password' => $user->plain_password,
            ]);

            $mail->selectFolder('Sent');

            $messageCount = $mail->countMessages();

            $messages = [];

            foreach ($mail as $messageNum => $message) {
                $has_attachment = false;
                if (strpos($message->contentType, 'multipart/mixed') !== false) {
                    $has_attachment = true;
                }
                array_push($messages, array(
                        'from'=>$message->from,
                        "subject"=>$message->subject,
                        'date'=>$message->date,
                        'read'=>$message->hasFlag(Storage::FLAG_SEEN),
                        'number'=>$messageNum,
                        'has_attachment' => $has_attachment
                    )
                );
            }
        }

        catch (\Exception $e) {
            $messages = [];
            $messageCount = 0;
        }

        return json_encode(array('messages' => $messages, 'message_count' => $messageCount));
    }

    public function syncDrafts($request, $response) {

        $user = $this->auth->user();

        try {
            $mail = new Imap([
                'host'     => 'mail.unodentity.com',
                'user'     => $user->email,
                'password' => $user->plain_password,
            ]);

            $mail->selectFolder('Drafts');

            $messages = [];
            $messageCount = 0;

            foreach ($mail as $messageNum => $message) {
                $has_attachment = false;
                array_push($messages, array(
                        'from'=>$message->from,
                        "subject"=>$message->subject,
                        'date'=>$message->date,
                        'read'=>$message->hasFlag(Storage::FLAG_SEEN),
                        'number'=>$messageNum,
                        'has_attachment' => $has_attachment
                    )
                );
            }
        }
        catch (\Exception $e) {
            $messages = [];
            $messageCount = 0;
        }

        return json_encode(array('messages' => $messages, 'message_count' => $messageCount));
    }

    public function syncTrash($request, $response) {

        $user = $this->auth->user();

        try {
            $mail = new Imap([
                'host'     => 'mail.unodentity.com',
                'user'     => $user->email,
                'password' => $user->plain_password,
            ]);

            $mail->selectFolder('Trash');

            $messageCount = $mail->countMessages();

            $messages = [];

            foreach ($mail as $messageNum => $message) {
                $has_attachment = false;
                if (strpos($message->contentType, 'multipart/mixed') !== false) {
                    $has_attachment = true;
                }
                array_push($messages, array(
                        'from'=>$message->from,
                        "subject"=>$message->subject,
                        'date'=>$message->date,
                        'read'=>$message->hasFlag(Storage::FLAG_SEEN),
                        'number'=>$messageNum,
                        'has_attachment' => $has_attachment
                    )
                );
            }
        }

        catch (\Exception $e) {
            $messages = [];
            $messageCount = 0;
        }

        return json_encode(array('messages' => $messages, 'message_count' => $messageCount));
    }

    public function syncSpam($request, $response) {

        $user = $this->auth->user();

        try {
            $mail = new Imap([
                'host'     => 'mail.unodentity.com',
                'user'     => $user->email,
                'password' => $user->plain_password,
            ]);

            $mail->selectFolder('Spam');

            $messageCount = $mail->countMessages();

            $messages = [];

            foreach ($mail as $messageNum => $message) {
                $has_attachment = false;
                if (strpos($message->contentType, 'multipart/mixed') !== false) {
                    $has_attachment = true;
                }
                array_push($messages, array(
                        'from'=>$message->from,
                        "subject"=>$message->subject,
                        'date'=>$message->date,
                        'read'=>$message->hasFlag(Storage::FLAG_SEEN),
                        'number'=>$messageNum,
                        'has_attachment' => $has_attachment
                    )
                );
            }
        }

        catch (\Exception $e) {
            $messages = [];
            $messageCount = 0;
        }

        return json_encode(array('messages' => $messages, 'message_count' => $messageCount));
    }

    public function searchFolder($request, $response)
    {

        $user = $this->auth->user();

        try {

            $conn = imap_open('{mail.unodentity.com:143/novalidate-cert}INBOX', $user->email, $user->plain_password);

            $query = imap_search($conn, 'SUBJECT '. $request->getParam("query").'', SE_UID);

            if ($query == false) {
                return json_encode(array('messages' => [], 'message_count' => 0));
            }

            $mail = new Imap([
                'host'     => 'mail.unodentity.com',
                'user'     => $user->email,
                'password' => $user->plain_password,
            ]);

            $messages = array();

            foreach ($query as $num) {
                $msg = $mail->getMessage($num);
                array_push($messages, array(
                        'from'=>$msg->from,
                        "subject"=>$msg->subject,
                        'date'=>$msg->date,
                        'read'=>$msg->hasFlag(Storage::FLAG_SEEN),
                        'number'=>$num)
                );

            }

            return json_encode(array('messages' => $messages, 'message_count' => count($query)));

        } catch (\Exception $e) {
            return json_encode(array('messages' => [], 'message_count' => 0));
        }
    }
}