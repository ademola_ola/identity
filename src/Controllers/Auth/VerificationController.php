<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/25/16
 * Time: 11:19 PM
 */

namespace App\Controllers\Auth;

use App\Controllers\BaseController;
use App\Models\AuthorizationToken;

class VerificationController extends BaseController
{

    public function getPage($request, $response) {

        return $this->container->view->render($response, 'templates/auth/verify.twig');
    }
}