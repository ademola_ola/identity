<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/27/16
 * Time: 12:09 PM
 */

namespace App\Controllers\Auth;

use Respect\Validation\Validator as Val;
use App\Controllers\BaseController;
use App\Models\AuthorizationToken;
use App\Models\User;
use App\Models\UserDetail;
use App\Services\Messaging;
use App\Services\Utils;
use App\Services\Index;
use App\Models\EmailAccount;
use App\Models\Credit;
use Twilio\Rest\Client;
use App\Models\UserPreference;

$settings = require __DIR__. '/../../settings.php';
$GLOBALS['settings'] = $settings;

class ConfirmationController extends BaseController
{

    public function getPage($request, $response) {

        $messages = $this->flash->getMessages();

        if (!isset($_SESSION['phone'])) {
            $this->flash->addMessage('error', 'Token has expired');
            return $response->withRedirect($this->router->pathFor('index'));
        }

        $phone = $_SESSION['phone'];

        return $this->container->view->render($response, 'alpha/confirmation.twig', [
            'phone' => $phone,
            'countries' => json_encode(Utils::load_codes()),
            'messages' => $messages
        ]);
    }

    public function postData($request, $response) {

        $validation = $this->validator->validate($request, [
            'token' => Val::noWhitespace()->notEmpty(),
            'password' => Val::notEmpty()->noWhitespace(),
        ]);

        $phone = $_SESSION['phone'];
        $token = $request->getParam('token');

        $match = ['access_token' => $token, 'phone' => $phone];

        $auth_token = AuthorizationToken::where($match)->first();
        
        if (!$auth_token || $validation->failed()) {
            $this->flash->addMessage('error', 'This authorization token is invalid or has expired');
            return $response->withRedirect($this->router->pathFor('confirm'));
        }

        $user = User::where('phone', $phone)->first();

        if ($user) {
            $this->flash->addMessage('error', 'An account with this phone number already exists');
            return $response->withRedirect($this->router->pathFor('confirm'));
        }

        $user = User::create([
            "phone" => $phone,
            "password"=> openssl_digest($request->getParam("password"), 'sha512'),
        ]);

        $hostname = $this->container->get('settings')['default_hostname'];
        $uid = $this->container->get('settings')['uid'];
        $gid = $this->container->get('settings')['gid'];

        $email = substr($user->phone.'@'.$hostname, 1);

        $details = UserDetail::create([
            "phone" => $user->phone,
            "email" => $email,
            "website_url" => $user->phone.'.'.$hostname,
            "password" => $user->password,
            "plain_password" => $request->getParam("password"),
            "maildir" => $user->phone,
            "uid"=> $uid,
            "gid" => $gid,
            "realname"=> "",
            "user_id" => $user->id
        ]);

        EmailAccount::create([
            "email" => $email,
            "mail_server" => "unodentity",
            "access_token" => '',
            "phone" => $user->phone,
            "default_sender" => true
        ]);

        UserPreference::create([
            'user_id'=>$user->id,
            'signature'=>' ',
            'undo_timer'=>1
        ]);

        $this->auth->attempt($user->phone, $request->getParam("password"));
        
//        Messaging::send_confirmation_messages($details);
        Messaging::send_confirmation_via_twilio($details);
        
        Credit::create(array('units'=> 5, 'phone'=>$user->phone));

        unset($_SESSION['phone']);

        $auth_token->expire();

        try {

            $es = new Index();
            $es->index_object('user_detail', substr($details->phone, 1), array('phone'=>substr($details->phone, 1), 'email'=>$details->email, 'website_url'=>$details->website_url, 'realname'=>$details->realname, 'user_id'=>$details->user_id));
        }

        catch (\Exception $e) {}
            
        return $response->withRedirect($this->router->pathFor('index'));
    }

    public function confirmToken($request, $response) {

        $phone = $request->getParam('phone');
        $token = $request->getParam('token');

        $match = ['access_token' => $token, 'phone' => $phone];

        $auth_token = AuthorizationToken::where($match)->first();

        if ($auth_token && !$auth_token->is_expired) {
            return $response->withJson(array('response'=> 'success'), 201);
        }

        return $response->withJson(array('response'=> 'error'), 201);
    }

    public function verifyNumber($request, $response) {

        $phone = $request->getParam('phone');

        $sid = $GLOBALS['settings']['settings']['twilio_sid'];
        $twilio_token = $GLOBALS['settings']['settings']['twilio_token'];

        $client = new Client($sid, $twilio_token);

        try {
            $number = $client->lookups->phoneNumbers($phone)->fetch(array("type" => "carrier"));
            if ($number->carrier["type"]) {
                return $response->withJson(array('response'=> 'success'), 201);
            }
            else {
                return $response->withJson(array('response'=> 'error'), 201);
            }
        }
        catch(\Exception $e) {
            return $response->withJson(array('response'=> 'error'), 201);
        }
    }

    public function resendToken($request, $response) {

        $phone = $request->getParam('phone');

        $match = ['is_expired' => false, 'phone' => $phone];

        $auth_token = AuthorizationToken::where($match)->first();

        if (!$auth_token) {
            unset($_SESSION['phone']);
            return $response->withJson(array('response'=> 'error'), 201);
        }

//        Messaging::send_verification_sms($phone, $auth_token->access_token);
        Messaging::send_via_twilio($phone, $auth_token->access_token);

        return $response->withJson(array('response'=> 'success'), 201);
    }
}