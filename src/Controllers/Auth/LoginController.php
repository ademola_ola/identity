<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 3:51 PM
 */

namespace App\Controllers\Auth;

use Respect\Validation\Validator as Val;
use App\Controllers\BaseController;
use App\Services\Utils;
use App\Helpers\Redirect;

class LoginController extends BaseController
{

    public function getLogin($request, $response) {

        $messages = $this->flash->getMessages();

        return $this->view->render($response, 'alpha/index.twig', [
            'countries' => json_encode(Utils::load_codes()),
            'messages' => $messages
        ]);
    }

    public function postLogin($request, $response) {

        $auth = $this->container->auth->attempt(
            (string)$request->getParam('countryCode')."". (string)(int)$request->getParam('phone'),
            $request->getParam('password')
        );
        
        if(!$auth) {
            $this->flash->addMessage('error', 'Invalid username/password combination');
            Redirect::redirect($this->router->pathFor('login'));
//            return $response->withRedirect($this->router->pathFor('login'));
        }

//        return $response->withRedirect($this->router->pathFor('landing'));
        Redirect::redirect($this->router->pathFor('landing'));
    }
}