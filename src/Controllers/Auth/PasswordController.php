<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/9/16
 * Time: 4:45 PM
 */

namespace App\Controllers\Auth;

use App\Models\UserDetail;
use App\Services\CodeUtility;
use Respect\Validation\Validator as Val;
use App\Controllers\BaseController;
use App\Models\User;


class PasswordController extends BaseController
{
    /**
     * @return mixed
     */
    public function getChangePassword($request, $response)
    {
        return $this->view->render($response, 'templates/auth/password/change.twig');

    }

    public function postChangePassword($request, $response) {


        $user_detail = $this->auth->user();

        $validation = $this->validator->validate($request, [
            'current_password' => Val::noWhitespace()->notEmpty(),
            'new_password' => Val::notEmpty()->noWhitespace(),
            'repeat_password' => Val::notEmpty()->equals('new_password')
        ]);


        if ($validation->failed()) {
//            return $response->withRedirect($this->router->pathFor('settings'));
            CodeUtility::returnMessage(405,'error','Missing Fields',"Please fill all fields.");
        }

        if($user_detail->plain_password!==$request->getParam('current_password')){
            return CodeUtility::returnMessage(405,'error','Invalid Current Field',"You have entered an incorrect password");
        }

        if(strlen($request->getParam('new_password'))<=6){
            return CodeUtility::returnMessage(405,'error','Invalid Current Field',
                "Your New Password is less than Six (6) Characters");
        }

        $new_password=$request->getParam('new_password');
        $repeat_password=$request->getParam('repeat_password');


        $db=$this->container->db->connection()->getPdo();
        $db->beginTransaction();

        try{

            $user_detail->setPassword($request->getParam('new_password'));

            $user = User::where('phone', $user_detail->phone)->first();
            $user->setPassword($request->getParam('new_password'));

            $user_info=User::find($user_detail->id)->userDetail;
            $user_detail->plain_password=$new_password;

            $user_info_sv=$user_info->save();

            if(!$user_info_sv){
                CodeUtility::returnMessage(405,'error','Password doesnt match',"Passwords does not match");
            }

            $db->commit();
            return CodeUtility::returnMessage(200,'success','Password Changed','Your Password has been updated successfully');
        }
        catch(Exception $ex){
            $db->rollBack();
            CodeUtility::log_info($ex-getMessage());
        }
    }
}