<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/5/16
 * Time: 1:08 AM
 */

namespace App\Controllers\Auth;

use App\Controllers\BaseController;
use App\Services\Utils;
use App\Services\Token;
use App\Models\User;
use App\Services\Messaging;

class ForgotController extends BaseController
{
    public function getPage($request, $response) {
        return $this->view->render($response, 'templates/auth/forgot.twig', [
            'countries' => Utils::load_codes()
        ]);
    }

    public function postPage($request, $response) {
        
        // insert logic to send out reset action here
        $device = (string)$request->getParam('countryCode')."". (string)(int)$request->getParam('phone');

        $user = User::where('phone', $device)->first();

        if (!$user) {
            $error = 'An account with this phone number was not found';
            return $this->view->render($response, 'templates/auth/forgot.twig', [
                'countries' => Utils::load_codes(),
                'error' => $error
            ]);
        }

        $token = Token::generateToken($device);

        Messaging::send_verification_sms($device, $token);
        
        return $response->withRedirect($this->router->pathFor('change'));
    }

}