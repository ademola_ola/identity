<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 3:33 PM
 */
namespace App\Controllers\Auth;

use App\Controllers\BaseController;
use App\Models\User;
use Respect\Validation\Validator as Val;
use App\Services\Token;
use App\Services\Messaging;
use App\Services\Utils;
use App\Helpers\Redirect;

class AuthController extends BaseController
{

    public function getSignUp($request, $response) {

        $messages = $this->flash->getMessages();

        return $this->view->render($response, 'alpha/register.twig', [
            'countries' => json_encode(Utils::load_codes()),
            'messages' => $messages
        ]);
    }

    public function postSignUp($request, $response) {
        
        $validation = $this->validator->validate($request, [
            'phone' => Val::noWhitespace()->notEmpty()->phone()->phoneAvailable(),
            'countryCode'=>Val::notEmpty()
        ]);


        if ($validation->failed()) {
            $this->flash->addMessage('error', 'Invalid input parameters');
            Redirect::redirect($this->router->pathFor('register'));
//            return $response->withRedirect($this->router->pathFor('register'));
        }

        $device = (string)$request->getParam('countryCode')."". (string)(int)$request->getParam('phone');

        
        $user = User::where('phone', $device)->first();

        
        if ($user) {
            $this->flash->addMessage('error', 'An account with this number already exists.');
            Redirect::redirect($this->router->pathFor('register'));
//            return $response->withRedirect($this->router->pathFor('register'));
        }
        
        $token = Token::generateToken($device);
        
//        Messaging::send_verification_sms($device, $token);

        Messaging::send_via_twilio($device, $token);

        Redirect::redirect($this->router->pathFor('confirm'));

//        return $response->withRedirect($this->router->pathFor('confirm'));
    }

    public function getLogOut($request, $response) {

        $this->auth->logout();

        Redirect::redirect($this->router->pathFor('index'));
//        return $response->withRedirect($this->router->pathFor('index'));
    }
}