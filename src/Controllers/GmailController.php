<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/16/16
 * Time: 3:14 PM
 */

namespace App\Controllers;
include "Helper.php";
$GLOBALS['settings'] = require __DIR__ . "/../settings.php";
use App\Models\EmailAccount;
use Google_Client;
use Google_Service_Gmail;
use Predis\Client;

define('SCOPES', implode(' ', array(
        Google_Service_Gmail::GMAIL_READONLY)
));

define('CLIENT_SECRET_PATH', __DIR__ . '/../../clients.json');

class GmailController extends BaseController
{

    public function requestAccess($request, $response) {

        $settings = $GLOBALS['settings'];
        $login_url = $settings['settings']['gmail']['login_url'];
        return file_get_contents($login_url);

    }
    
    public function redirectUri($request, $response) {
        $settings = $GLOBALS['settings'];
        $gmail = $settings['settings']['gmail'];
        $header = array( "Content-Type: application/x-www-form-urlencoded" );

        $data = http_build_query(
            array(
                'code' => str_replace("#", null, $_GET['code']),
                'client_id' => $gmail['client_id'],
                'client_secret' => $gmail['client_secret'],
                'redirect_uri' => $gmail['redirect_uri'],
                'grant_type' => 'authorization_code'
            )
        );

        $url = "https://www.googleapis.com/oauth2/v4/token";

        $result = HTTP(1, $url, $header, $data);

        $access_token = $result['access_token'];
        
        $info = HTTP(0, "https://www.googleapis.com/gmail/v1/users/me/profile", array("Authorization: Bearer $access_token"), 0);
        $email = $info['emailAddress'];

        $user = $this->auth->user();

        $account = EmailAccount::where('email', $email)->first();
        
        if ($account) {
           $account->setAccessToken($access_token);
        }
        
        else {
            EmailAccount::create([
                "email" => $email,
                "mail_server" => "gmail",
                "access_token" => $access_token,
                "phone" => $user->phone
            ]);
        }
        
        return $response->withRedirect($this->router->pathFor('landing'));
    }

    public function readMail($request, $response) {
        $client = new Google_Client();
        $client->setApplicationName('1Identity');
        $client->addScope(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH);
        $client->setAccessType('offline');

        $authUrl = $client->createAuthUrl();
        return file_get_contents($authUrl);

//        if ($redis->get('access_token')) {
//            $access_token = $redis->get('access_token');
//        }
//        else {
//            $authUrl = $client->createAuthUrl();
//            return file_get_contents($authUrl);
//        }

//        $client->setAccessToken($access_token);
//
//        if ($client->isAccessTokenExpired()) {
//            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
//            $redis->set('access_token', $client->getAccessToken());
//        }
//
//        return $client;
    }

    public function redirectRequest($request, $response) {

        $client = new Google_Client();
        $client->setApplicationName('1Identity');
        $client->addScope(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH);
        $client->setAccessType('offline');

        $redis = new Client();

        $code = $request->getParam('code');
        $resp = $client->fetchAccessTokenWithAuthCode($code);

        if ($resp['error']){
            return $response->withRedirect($this->router->pathFor('landing'));
        }

        $accessToken = $resp['access_token'];

        $redis->set('access_token', $accessToken);

        $info = HTTP(0, "https://www.googleapis.com/gmail/v1/users/me/profile", array("Authorization: Bearer $accessToken"), 0);
        $email = $info['emailAddress'];

        $user = $this->auth->user();

        $account = EmailAccount::where('email', $email)->first();

        if ($account) {
            $account->setAccessToken($accessToken);
        }

        else {
            EmailAccount::create([
                "email" => $email,
                "mail_server" => "gmail",
                "access_token" => $accessToken,
                "phone" => $user->phone
            ]);
        }
        
        return $response->withRedirect($this->router->pathFor('landing'));
    }
}