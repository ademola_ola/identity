<?php
/**
 * Created by PhpStorm.
 * User: yusufbnahmad
 * Date: 15/08/16
 * Time: 11:34
 */

namespace App\Controllers;


class AppController extends BaseController
{

    public function getPage($request, $response){

        return $this->view->render($response, 'templates/apps.twig');
    }
}