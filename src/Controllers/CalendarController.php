<?php

/**
 * Created by PhpStorm.
 * User: stikks-workstation
 * Date: 12/2/16
 * Time: 8:56 AM
 */
namespace App\Controllers;
use App\Models\EmailAccount;
use App\Models\Events;
use Google_Client;
use Predis\Client;
use DateTime;
use Google_Service_Calendar;

define('SCOPES', implode(' ', array(
        Google_Service_Calendar::CALENDAR_READONLY)
));

class CalendarController extends BaseController
{

    public function getPage($request, $response) {

        $user = $this->auth->user();

        $events = Events::where('phone', $user->phone)->get();

        $link = $request->getUri()->getBaseUrl();

        return $this->view->render($response, 'templates/admin/contacts.twig', [
            'user' => $user,
            'events' => $events,
            'link' => $link
        ]);
    }

    public function syncGoogle($request, $response) {

        $client = new Google_Client();
        $client -> setApplicationName('1Identity');
        $client->setScopes(SCOPES);

        $client -> setClientId('750074493137-sskup8gsc9g3hi95od91e8s7aot2m9m9.apps.googleusercontent.com');
        $client -> setClientSecret("v-ESmPFRFPD6wCq6JqUrwCKG");
//        $client -> setRedirectUri('http://localhost:9089/google/calendar_feedback');
        $client -> setRedirectUri('http://unoidentity.com:8900/google/calendar_feedback');
        $client -> setAccessType('offline');

        $authUrl = $client->createAuthUrl();

        return file_get_contents($authUrl);
    }

    public function redirectRequest($request, $response) {


        $client = new Google_Client();
        $client -> setApplicationName('1Identity');
        $client->setScopes(SCOPES);

        $client -> setClientId('750074493137-sskup8gsc9g3hi95od91e8s7aot2m9m9.apps.googleusercontent.com');
        $client -> setClientSecret("v-ESmPFRFPD6wCq6JqUrwCKG");
//        $client -> setRedirectUri('http://localhost:9089/google/calendar_feedback');
        $client -> setRedirectUri('http://unoidentity.com:8900/google/calendar_feedback');
        $client -> setAccessType('offline');

        $redis = new Client();

        $code = $request->getParam('code');
        $resp = $client->fetchAccessTokenWithAuthCode($code);

        if (isset($resp['error'])){
            return $response->withRedirect($this->router->pathFor('landing'));
//            return $response->withRedirect('/home');
        }

        $service = new Google_Service_Calendar($client);

        $calendarId = 'primary';
        $optParams = array(
            'orderBy' => 'startTime',
            'singleEvents' => TRUE,
            'timeMin' => date('c'),
        );

        $user = $this->auth->user();

        $results = $service->events->listEvents($calendarId, $optParams);

        $d = strtotime("+1 Year");
        $limit = date("Y-m-d", $d);

        foreach ($results->getItems() as $event) {

            if ($event->start->dateTime && ($limit > $event->start->dateTime)) {
                $value = [
                    "phone" => $user->phone,
                    "start_date" => new DateTime($event->start->dateTime),
                    "end_date"  => new DateTime($event->end->dateTime),
                    "link" => $event->htmlLink,
                    "location" => $event->location,
                    "name" => $event->summary,
                    "description" => $event->description,
                    "uniqueid" => $event->id
                ];

                $match = ['phone' => $user->phone, 'uniqueid' => $event->id];

                $account = Events::where($match)->first();

                if (!$account) {
                    Events::create($value);
                }
            }
        }

        return $response->withRedirect($this->router->pathFor('landing'));
//        return $response->withRedirect('/home');
    }
}