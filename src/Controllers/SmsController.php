<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/30/16
 * Time: 10:22 AM
 */
namespace App\Controllers;
use App\Services\Messaging;
use App\Models\Messages;

class SmsController extends BaseController
{

    public function postData($request, $response){
        
        $user = $this->auth->user();

        $credit = Credit::where('phone', $user->phone)->first();
        
        if ($credit->units = 0) {
            return $response->withStatus(200)->write('No units available');
        }

        $phone = $request->getParam('phone');
        $message = $request->getParam('message');
        
        $resp = Messaging::send_sms($user->phone, $phone, $message);
        
        if ($resp == true) {
            return $response->withStatus(200)->write('success');
//            return $response->withRedirect($this->router->pathFor('confirm', array('success', "Message successfully sent")));
        }

        return $response->withStatus(200)->write('error');
//        return $response->withRedirect($this->router->pathFor('landing', array('error', 'Message not sent')));
    }
}
