<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/30/16
 * Time: 10:22 AM
 */
namespace App\Controllers;


class CallController extends BaseController
{

    public function getPage($request, $response){

        return $this->view->render($response, 'templates/call.twig');
    }
}