<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 1:43 PM
 */
namespace App\Controllers;

use App\Services\CodeUtility;
use App\Services\Utils;
use App\Models\User;
use App\Services\Messaging;
use App\Services\Token;
use Respect\Validation\Validator as Val;
use Zend\Mail\Storage\Imap;
use App\Models\Feeds;
use App\Helpers\Redirect;

class IndexController extends BaseController
{

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function index($request, $response){

        $messages = $this->flash->getMessages();

        return $this->view->render($response, 'alpha/home.twig', [
            'countries' => json_encode(Utils::load_codes()),
            'messages' => $messages
        ]);
    }

    /**
     * @param $request
     * @param $response
     * @return mixed
     */
    public function postData($request, $response) {

        $validation = $this->validator->validate($request, [
            'phone' => Val::noWhitespace()->notEmpty()->phone()->phoneAvailable(),
            'countryCode'=>Val::notEmpty()
        ]);


        if ($validation->failed()) {
            $this->flash->addMessage('error', 'Invalid input parameter');
//            return $response->withRedirect($this->router->pathFor('index'));
            Redirect::redirect($this->router->pathFor('index'));
        }

        $device = (string)$request->getParam('countryCode')."". (string)(int)$request->getParam('phone');

        $user = User::where('phone', $device)->first();

        if ($user) {
            $this->flash->addMessage('error', 'An account with this phone number already exists');
            Redirect::redirect($this->router->pathFor('index'));
//            return $response->withRedirect($this->router->pathFor('index'));
        }

        $token = Token::generateToken($device);

        Messaging::send_verification_sms($device, $token);

        Redirect::redirect($this->router->pathFor('confirm'));
//        return $response->withRedirect($this->router->pathFor('confirm'));
    }
}