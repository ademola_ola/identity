<?php
/**
 * Created by PhpStorm.
 * User: codesmata
 * Date: 1/11/17
 * Time: 9:42 AM
 */

namespace App\Controllers;

use App\Models\User;
use App\Services\Queueing;

class TestController extends BaseController
{

    private $c;

    function __construct($container)
    {
        $this->c=$container;
    }

    function index(){
        //this queues messages
        $msg=[
            'sender'=>'Sender',
            'cc'=>'cc,cc,cc,cc',
            'bcc'=>'bcc,bcc,bccc',
            'recipient'=>'Recipient',
            'subject'=>'Subject',
            'msg'=>'Message'
        ];

        //$this->c->logger->info("hello world!!");
        $this->c->logger->addInfo(json_encode("Hello World"));
        $queue=new Queueing();
        $queue->queueMessage($msg);

        echo "Message Queued Successfully!!<br />";
    }
}