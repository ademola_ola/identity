<?php
/**
 * Created by PhpStorm.
 * User: stikks-workstation
 * Date: 1/26/17
 * Time: 5:55 PM
 */

namespace App\Helpers;


class Utils
{
    public static function FileExt($contentType) {
        $map = array(
            'application/pdf'   => '.pdf',
            'application/zip'   => '.zip',
            'image/gif'         => '.gif',
            'image/jpeg'        => '.jpg',
            'image/png'         => '.png',
            'text/css'          => '.css',
            'text/html'         => '.html',
            'text/javascript'   => '.js',
            'text/plain'        => '.txt',
            'text/xml'          => '.xml',
        );
        if (isset($map[$contentType]))
        {
            return $map[$contentType];
        }

        $pieces = explode('/', $contentType);
        return '.' . array_pop($pieces);
    }
}