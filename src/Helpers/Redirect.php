<?php

/**
 * Created by PhpStorm.
 * User: stikks-workstation
 * Date: 1/25/17
 * Time: 10:11 AM
 */
namespace App\Helpers;

class Redirect
{

    static public function redirect($url)
    {
        header('Location: ' . $url, true);
        die();
    }
}