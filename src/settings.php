<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 1:02 PM
 */
return [
    'settings' => [
        'domain' => 'http://unodentity.com',
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header


        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'level'=>Monolog\Logger::DEBUG,
            'path' => __DIR__.'./logs/app.log',
        ],
        'db' => [
            'driver' => 'pgsql',
            'host' => 'localhost',
            'username' => 'mailreader',
            'password' => 'mailreader',
            'database' => 'oneidentity',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci'
        ],
        'uid'=> 200,
        'gid'=> 1000,
        'default_hostname' => 'unodentity.com',
        'mail_host' => 'imap.unodentity.com',
        'smtp_server' => [
            'security' => 'Non-Encrypted',
            'authentication' => 'AUTH',
            'port' => 25
        ],
        'imap_server' => [
            'security' => 'Non-Encrypted',
            'authentication' => 'AUTH',
            'port' => 143
        ],
        'sms_api' => 'http://www.1960sms.com/api/send/?user=identity&pass=identity&from=1Identiti&to=',
        'gmail' => [
//            'scope' => "https://www.googleapis.com/auth/gmail.compose",
            'scope' => 'https://www.googleapis.com/auth/gmail.readonly',
            'redirect_uri' => "http://unodentity.com/user/gmail/feedback",
//            'client_id' => "287893726188-jfplrk23fp3fpb6p6sjmtorklnaa93qa.apps.googleusercontent.com",
            'client_id' => '750074493137-sskup8gsc9g3hi95od91e8s7aot2m9m9.apps.googleusercontent.com',
//            'client_secret' => "C0_-mLHwjDx3-TYCALgnxDPP",
            'client_secret' => 'v-ESmPFRFPD6wCq6JqUrwCKG',
            'login_url' => "https://accounts.google.com/o/oauth2/v2/auth?scope=https://mail.google.com/&response_type=code&redirect_uri=http://unodentity.com/user/gmail/feedback&client_id=287893726188-hup8bm8qd02eeds83hh3sjtpubc3q57i.apps.googleusercontent.com"
        ],
        "default_index" => "unodentity",
        "twilio_sid" => 'AC4ac49a6bd52a22902877738863f3ae90',
        "twilio_token" => 'f05059693d4cf75c28ef8a3390cb405c',
        "twilio_sender" => '+12074820335',
        "sendgrid_api_key" => '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',
        'iam_username' => 'ses-smtp-user.20170131-161730'
    ]    
];
