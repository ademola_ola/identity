<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 1:05 PM
 */
use Respect\Validation\Validator as Val;
use App\Auth\Auth;

$container = $app->getContainer();
$capsule = new \Illuminate\Database\Capsule\Manager;

$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();


// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger=new Monolog\Logger('logger');
    $filename='./logs/app.log';
    $stream=new Monolog\Handler\StreamHandler($filename,Monolog\Logger::DEBUG);
   // $fingersCrossed=new \Monolog\Handler\FingersCrossedHandler($stream,Monolog\Logger::ERROR);
    $logger->pushHandler($stream);
    //$logger->pushHandler($fingersCrossed);

    return $logger;
};

$container['db'] = function ($c) use ($capsule) {
    return $capsule;
};

//$container['db'] = function ($c) {
//    $db = $c->get('settings')['db'];
////    $db = $c['settings']['db'];
//    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname']. ";charset=utf8",
//        $db['user'], $db['pass']);
//    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
//    $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//    return $pdo;
//};

$container['auth'] = function ($c) {
    return new Auth;
};

$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../resources/views', [
        'cache' => false,
    ]);
    $view->addExtension(new \Slim\Views\TwigExtension(
        $c->router,
        $c->request->getUri()
    ));

    $view->getEnvironment()->addGlobal('auth', [
        'check' => $c->auth->check(),
//        'user' => $c->auth->user()
    ]);

    $view->getEnvironment()->addGlobal('flash', $c->flash);

    $filter = new Twig_SimpleFilter('ago', function ($datetime) {

        $time = time() - strtotime($datetime);

        $units = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($units as $unit => $val) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return ($val == 'second')? 'a few seconds ago' :
                (($numberOfUnits>1) ? $numberOfUnits : 'a')
                .' '.$val.(($numberOfUnits>1) ? 's' : '').' ago';
        }

    });

    $twig = $view->getEnvironment();//because I'm using Twig in Slim framework
    $twig->addFilter($filter);

    return $view;
};

$container['validator'] = function ($c) {
  return new App\Validation\Validator;
};

$container['csrf'] = function ($c) {
    return new Slim\Csrf\Guard;
};

//$app->add($container->get('csrf'));

$container["IndexController"] = function ($c) {
    return new \App\Controllers\IndexController($c);
};

$container["VerificationController"] = function ($c) {
    return new \App\Controllers\Auth\VerificationController($c);
};

$container["ConfirmationController"] = function ($c) {
    return new \App\Controllers\Auth\ConfirmationController($c);
};

$container["AppController"] = function ($c) {
    return new \App\Controllers\AppController($c);
};

$container["NotificationController"] = function ($c) {
    return new \App\Controllers\NotificationController($c);
};

$container["PrivacyController"] = function ($c) {
    return new \App\Controllers\PrivacyController($c);
};

$container["AuthController"] = function ($c) {
    return new \App\Controllers\Auth\AuthController($c);
};

$container["PasswordController"] = function ($c) {
    return new \App\Controllers\Auth\PasswordController($c);
};

$container["LoginController"] = function ($c) {
    return new \App\Controllers\Auth\LoginController($c);
};

$container["ForgotController"] = function ($c) {
    return new \App\Controllers\Auth\ForgotController($c);
};

$container["MailController"] = function ($c) {
    return new \App\Controllers\MailController($c);
};

$container["LandingController"] = function ($c) {
    return new \App\Controllers\LandingController($c);
};

$container["SettingsController"] = function ($c) {
    return new \App\Controllers\SettingsController($c);
};

$container["SearchProfileController"] = function ($c) {
    return new \App\Controllers\SearchProfileController($c);
};

$container["SearchController"] = function ($c) {
    return new \App\Controllers\SearchController($c);
};

$container["SmsController"] = function ($c) {
    return new \App\Controllers\SmsController($c);
};

$container["GmailController"] = function ($c) {
    return new \App\Controllers\GmailController($c);
};

$container["YahooController"] = function ($c) {
    return new \App\Controllers\YahooController($c);
};

$container["EmailController"] = function ($c) {
    return new \App\Controllers\EmailController($c);
};

$container["ComposeController"] = function ($c) {
    return new \App\Controllers\ComposeController($c);
};

$container["ChatController"] = function ($c) {
    return new \App\Controllers\ChatController($c);
};

$container["ContactController"] = function ($c) {
    return new \App\Controllers\ContactController($c);
};

$container["CalendarController"] = function ($c) {
    return new \App\Controllers\CalendarController($c);
};

$container["ApiController"] = function ($c) {
    return new \App\Controllers\ApiController($c);
};

$container["EventController"] = function ($c) {
    return new \App\Controllers\EventController($c);
};
$container['TestController']=function($c){
    return new \App\Controllers\TestController($c);
};
$app->add(new \App\Middleware\ValidationErrorsMiddleware($container));
$app->add(new \App\Middleware\OldInputMiddleware($container));
//$app->add(new \App\Middleware\CsrfMiddleware($container));

Val::with('App\\Validation\\Rules\\');