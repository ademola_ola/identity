<?php
/**
 * Created by PhpStorm.
 * User: codesmata
 * Date: 1/17/17
 * Time: 11:20 AM
 */

namespace App\Services;

use App\Models\Group;
use App\Models\GroupMember;
use App\Models\User;

class GroupManagement
{
    /**
     * @param $user_id
     * @param $group_name
     * @return string
     */
    public function createNewGroup($user_id,$group_name){
        //validate the existence of the group for this user
        $groupCheck=Group::whereRaw('user_id=? and group_name=?',[$user_id,$group_name])->get();

        if(count($groupCheck)>0){
            return CodeUtility::returnMessage(500,'error','Group Exists','You have created this group before!!');
        }

        //proceed to create
        $group_crt=Group::create([
            'user_id'=>$user_id,
            'group_name'=>$group_name
        ]);

        if(!$group_crt){
            return CodeUtility::returnMessage(500,'error','Connection Error','An error occurred while creating this group!');
        }

        return CodeUtility::returnMessage(200,'success','Group Created',$group_name.' has been created successfully!!');
    }

    /**
     * @param $userid
     * @return string
     */
    public function listUserGroup($userid){

        $user_info=User::find($userid);

        if(count($user_info)<=0){
            return CodeUtility::returnMessage(405,'error','User Not Found','Invalid User ID');
        }

        return json_encode($user_info->groups);
    }
}