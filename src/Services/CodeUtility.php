<?php
/**
 * Created by PhpStorm.
 * User: codesmata
 * Date: 1/12/17
 * Time: 1:20 PM
 */

namespace App\Services;


class CodeUtility{

    function __construct(){ }

    static function log_info($msg){
        file_put_contents(__DIR__.'/logging/info_log.log',json_encode($msg).PHP_EOL,FILE_APPEND| LOCK_EX);
    }

    static function returnMessage($code,$status,$description,$msg){
        self::log_info($msg);
        $resp=[
            'code'=>$code,
            'status'=>$status,
            'description'=>$description,
            'message'=>$msg
        ];

        return json_encode($resp);
    }

    static function errorData($response,$code,$status,$description,$msg){
        $data=[
            'code'=>$code,
            'status'=>$status,
            'description'=>$description,
            'message'=>$msg
        ];

        return $response->withStatus(200)->write(json_encode($data));
    }

    static function sendMessage($response,$code,$status,$description,$msg){
        $data=[
            'code'=>$code,
            'status'=>$status,
            'description'=>$description,
            'message'=>$msg
        ];

        return $response->withStatus(200)->write(json_encode($data));
    }
}