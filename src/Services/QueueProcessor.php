<?php
/**
 * Created by PhpStorm.
 * User: codesmata
 * Date: 1/11/17
 * Time: 2:19 PM
 */

namespace App\Services;
require '../../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;


$connection =new AMQPStreamConnection('localhost',5672,'guest','guest');
$channel=$connection->channel();
$channel->queue_declare('mails',false,false,false,false);

$callBack=function($msg){
    Messaging::pre_process_send_email($msg->getBody());
};

echo '[*] Waiting for message exit wit CTRL+C'."\n";

$channel->basic_consume('mails','',false,true,false,false,$callBack);
while(count($channel->callbacks)){
    $channel->wait();
}

$channel->close();
$connection->close();