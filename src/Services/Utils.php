<?php
/**
 * Created by PhpStorm.
 * User: stikks-workstation
 * Date: 9/15/16
 * Time: 1:34 PM
 */

namespace App\Services;

class Utils
{
    static public function get_client_ip() {

        $ip_address = null;

        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ip_address = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ip_address = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ip_address = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ip_address = $_SERVER['REMOTE_ADDR'];

        return $ip_address;
    }

    static public function load_codes() {

        $json_file = json_decode(file_get_contents('codes.json'), true);
        return $json_file['countries'];
    }
}

