<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/25/16
 * Time: 11:13 PM
 */

namespace App\Services;

require __DIR__. '/../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
include __DIR__. '/../Controllers/Helper.php';
use Illuminate\Contracts\Logging\Log;
use PHPMailer;
use Requests;
use Google_Client;
use Google_Service_Gmail;
use Google_Service_Gmail_Message;
use Zend\Mail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part;
use Zend\Mime\Mime;
use App\Models\Messages;
use App\Models\Credit;
use Zend\Mail\Storage\Imap;
use App\Services\CodeUtility as util;
use Twilio\Rest\Client;
use App\Helpers\Utils;
use SendGrid;

$settings = require __DIR__. '/../settings.php';
$GLOBALS['settings'] = $settings;

final class Messaging
{

    /**
     * send_verification_sms($phone,$token)
     * send_sms($sender,$phone,$message)
     * check_credits($phone)
     * send_confirmation_message($user
     * send_url_sms($url)
     * send_mail($subject,$body,$address,$email,$name)
     * send_via_zend($subject,$body,$address,$from_name,$plain_password,$email,$file_path,$name)
     * saveSent($email,$password,$message)
     * send_via_gmail($account,$subject,$to,$message)
     * send_via_zend($subject,$body,$address,$from_name,$plain_password,$email,$filepaths,$name);
     */

    static public function send_verification_sms($phone, $token) {

        $message = "Type in ".$token.' to verify your phone number and complete the registration process. Thank you';
        $settings = $GLOBALS['settings'];

//        $request_url = "http://sponge.atp-sevas.com:8585/sevas/partner_send_sms_api?formatType=xml&puid=sponge_ext&ppwd=fileopen&psid=3338&pssc=32682&pm=".$message."&pr=".$phone;

        $request_url = $settings['settings']['sms_api']. $phone. '&msg='.$message;

        return static::send_url_sms($request_url);
    }

    static public function send_sms($sender, $phone, $message) {
        $settings = $GLOBALS['settings'];
        $request_url = $settings['settings']['sms_api']. $phone. '&msg='.$message;
        $send = static::send_url_sms($request_url);
        
        if ($send) {
            Messages::create([
                'phone' => $sender,
                'recipient'=>$phone,
                "text"=>$message,
            ]);
            return true;
        }
        return false;
    }

    static public function send_via_twilio($phone, $token) {
        $message = "Type in ".$token.' to verify your phone number and complete the registration process. Thank you';
        $settings = $GLOBALS['settings'];
        $sid = $settings['settings']['twilio_sid'];
        $twilio_token = $settings['settings']['twilio_token'];
        $sender = $settings['settings']['twilio_sender'];
        $client = new Client($sid, $twilio_token);

        try {
            $client->messages->create(
                $phone,
                array(
                    'from' => $sender,
                    'body' => $message
                )
            );
            return true;
        }
        catch(\Exception $e) {
            return false;
        }
    }

    static public function send_confirmation_via_twilio($user) {
        // send confirmation message welcoming you the platform

        $settings = $GLOBALS['settings'];

        $sid = $settings['settings']['twilio_sid'];
        $twilio_token = $settings['settings']['twilio_token'];
        $sender = $settings['settings']['twilio_sender'];
        $client = new Client($sid, $twilio_token);

        $domain = $settings['settings']['domain'];
        $url = ''.$domain.'/user';

        $message = "Welcome to 1Identiti, Visit ".$url.' to start exploring the extensive features on the platform. Thank you';

        try {
            $client->messages->create(
                $user->phone,
                array(
                    'from' => $sender,
                    'body' => $message
                )
            );
            static::send_via_sendgrid("hello@unodentity.com", [$user->email,], array(), array(), 'Welcome to 1Identiti', $message,  $user->phone);

            return true;
        }
        catch(\Exception $e) {
            return false;
        }
    }

    static public function send_confirmation_messages($user) {
        // send confirmation message welcoming you the platform

        $settings = $GLOBALS['settings'];

        $domain = $settings['settings']['domain'];
        $url = ''.$domain.'/user';
        $message = "Welcome to 1Identiti, Visit ".$url.' to start exploring the extensive features on the platform. Thank you';

        //send sms
//        $request_url = "http://sponge.atp-sevas.com:8585/sevas/partner_send_sms_api?formatType=xml&puid=sponge_ext&ppwd=fileopen&psid=3338&pssc=32682&pm=".$message."&pr=".$user->phone;
        $request_url = $settings['settings']['sms_api']. $user->phone. '&msg='.$message;
        static::send_url_sms($request_url);

        //send email
//        static::send_mail('Welcome to One Identity', $message, "hello@unoidentity.com", $user->email, $user->phone);

        return true;
    }

    private function send_url_sms($url) {

        try {
            $response = Requests::get($url);
        }
        catch (\Exception $e) {
            return false;
        }

        if ($response->status_code == 200) {
            return true;
        }

        return false;

    }

    static public function send_mail($subject, $body, $address, $email, $name)
    {
        
        $mail = new PHPMailer;
        $mail->From = $address;

        $mail->addAddress($email, $name);
        $mail->addReplyTo($address);

        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->AltBody = $body;

        if(!$mail->send()) {
            return 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            return true;
        }
    }


    static public function saveSent($email, $password, $message) {

        $mail = new Imap([
            'host'     => 'mail.unodentity.com',
            'user'     => $email,
            'password' => $password,
        ]);

        $mail->selectFolder('Sent');

        $mail->appendMessage($message);

        return true;
    }

    static public function saveTrash($email, $password, $message) {

        $mail = new Imap([
            'host'     => 'mail.unodentity.com',
            'user'     => $email,
            'password' => $password,
        ]);

        $mail->selectFolder('Trash');

        $mail->appendMessage($message);

        return true;
    }
    
    static public function send_via_gmail($account, $subject, $to, $message) {

        $client = new Google_Client();

        $client->setAccessToken($account);

        $client->addScope("https://www.googleapis.com/auth/gmail.compose");
        $service = new Google_Service_Gmail($client);

        $mime = rtrim(strtr(base64_encode($message), '+/', '-_'), '=');
        $msg = new Google_Service_Gmail_Message();
        $msg->setRaw($mime);
        $service->users_messages->send("me", $msg);
        
        return true;
    }

    /**
     * @param $subject
     * @param $body
     * @param $address
     * @param $from_name
     * @param $plain_password
     * @param $email
     * @param array $filePaths
     * @param null $name
     * @return bool
     */
    static public function send_via_zend($subject, $body, $address, $from_name, $plain_password, $email, $name=null,$filePaths=array(),$ccs=[],$bccs=[]) {

        $html = new Part($body);
        $html->type = "text/html";

        $bodyText = new MimeMessage();
        $bodyText->setParts(array($html));

        $mail = new Mail\Message();
        $mail->setBody($bodyText);

        $mail->setFrom($address, $from_name);
        $mail->addTo($email, $name);
        $mail->setSubject($subject);

        //check if bcc and cc are not empty
        if(count($ccs)>0){
            foreach ($ccs as $cc){
                if($cc!==""){
                    $mail->addCc($cc);
                }
            }
        }

        //=========================ADDDING BCCs
        if(count($bccs)>0){
            foreach ($bccs as $bcc){
                if($bcc!==""){
                    $mail->addBcc($bcc);
                }
            }
        }

        //lets get the uploaded files url
        //we upload the attachments and get the new Path
        if(count($filePaths)>0){
            for($i=0; $i<count($filePaths); $i++){
                //echo file_get_contents('../../'.$filePaths[$i]);
                $mail->addAttachment('../../'.$filePaths[$i]);
            }
        }


        $transport = new Mail\Transport\Sendmail();
  //      $transport->send($mail);

//        static::saveSent($address, $plain_password, $mail->toString());

        util::log_info("All CCs and Bcc are added And files attached \n".$mail->toString());
        return true;
    }

    /**
     * @param $parameters
     *
     * pre-processes the request from the message queues accordingly....
     * @return message
     */
    static public function pre_process_send_email($parameters){

        //parameters retrieval
         $params=json_decode($parameters);


         $cc_list=[];
         $bcc_list=[];

            $sender=$params->sender;
            $sender_name=$params->sender_name;
            $plain_pword=$params->plain_password;
            $recipient=$params->recipient;
            $subject=$params->subject;
            $msg=$params->msg;
            $cc=$params->cc;
            $bcc=$params->bcc;
            $files=$params->files;

            //==============================get the list of ccs and bccs
            $cc_list=explode(',',$cc);
            $bcc_list=explode(',',$bcc);

            self::send_via_zend($subject,$msg,$sender,$sender_name,$plain_pword,$recipient,null,$files,$cc_list,$bcc_list);
        util::log_info("moving on");
    }


    /**
     * @param $oname
     * @param $newName
     *
     * @return String
     * This uploads the file
     */
    static public function uploadFile($oname,$newName){
        util::log_info(getcwd());
        if(!move_uploaded_file($oname,$newName)){
            echo 'Error!! Cannot Upload This File '.$newName;
        }else{
            return "Attachment Upload Successful!!";
        }
    }

    static public function send_via_sendgrid($password, $from, $subject, $body, $to=array(),$cc=array(),$bcc=array(), $from_name=null, $files=array()) {

        $sendgrid = new SendGrid('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');
        $mail = new SendGrid\Mail();
        $email = new SendGrid\Email($from_name, $from);
        $mail->setFrom($email);

        $mail->setSubject($subject);
        $personalization = new SendGrid\Personalization();

        foreach ($to as $item) {
            if (strlen($item) > 0) {
                $personalization->addTo(new SendGrid\Email(null, $item));
            }
        }

        if (count($cc)) {
            foreach ($cc as $item) {
                if (strlen($item) > 0) {
                    $personalization->addCc(new SendGrid\Email(null, $item));
                }
            }
        }

        if (count($bcc)) {
            foreach ($bcc as $item) {
                if (strlen($item) > 0) {
                    $personalization->addBcc(new SendGrid\Email(null, $item));
                }
            }
        }

        $mail->addPersonalization($personalization);
        $content = new SendGrid\Content("text/html", "<html><body>".$body."</body></html>");
        $mail->addContent($content);

        $footer = new SendGrid\Footer();
        $footer->setEnable(true);
        $footer->setText("Sent by 1Identiti");
        $footer->setHtml("<html><body>Sent by 1Identiti</body></html>");
        $reply_to = new SendGrid\ReplyTo($from);
        $mail->setReplyTo($reply_to);

        foreach ($files as $file) {

            $attachment = new SendGrid\Attachment();
            $attachment->setContent(base64_encode(file_get_contents($file['path'], 'r')));
            $attachment->setType(mime_content_type($file['path']));
            $attachment->setFilename($file['name']);
            $attachment->setDisposition("attachment");
            $mail->addAttachment($attachment);
        }

        $response = $sendgrid->client->mail()->send()->post($mail);

        if ($response->statusCode() == 202) {

            try {
                $html = new Part($body);
                $html->type = "text/html";

                $bodyText = new MimeMessage();
                $bodyText->setParts(array($html));

                $save_mail = new Mail\Message();
                $save_mail->setBody($bodyText);

                $save_mail->setFrom($from, $from_name);
                $save_mail->setSubject($subject);

                $_to = explode(",", $to[0]);
                if(count($_to)>0){
                    foreach ($_to as $t){
                        if(strlen($t)){
                            $save_mail->addTo($t);
                        }
                    }
                }

                //check if bcc and cc are not empty
                $_cc = explode(",", $cc[0]);
                if(count($_cc)>0){
                    foreach ($_cc as $c){
                        if(strlen($c)){
                            $save_mail->addCc($c);
                        }
                    }
                }

                //=========================ADDDING BCCs
                $_bcc = explode(",", $bcc[0]);
                if(count($_bcc)>0){
                    foreach ($_bcc as $bc){
                        if(strlen($bc)){
                            $save_mail->addBcc($bc);
                        }
                    }
                }

                self::saveSent($from, $password, $save_mail->toString());

                return true;
            } catch (\Exception $e) {
                return false;
            }
        }

        return false;
    }

}