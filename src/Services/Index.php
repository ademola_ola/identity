<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 9/20/16
 * Time: 11:59 PM
 */

namespace App\Services;
use Elasticsearch\ClientBuilder;

class Index {

    protected $client;
    protected $index;

    function __construct($index=null)
    {
        # initialize elastic search
        $this->client = ClientBuilder::create()->build();

        if ($index) {
            $this->create_index($index);
        }
        else {

            $settings = require __DIR__. '/../settings.php';
            $index = $settings['settings']['default_index'];

            try {

                $params = [
                    'index' => $index,
                    'body' => [
                        'settings' => [
                            'number_of_shards' => 2,
                            'number_of_replicas' => 0
                        ]
                    ]
                ];

                $this->client->indices()->create($params);
            }

            catch (\Exception $e) {

                $Params = array('index' => $index);
                $this->client->indices()->get($Params);
            }
        }

        $this->index = $index;

    }

    public function create_index($index) {

        $params = [
            'index' => $index,
            'body' => [
                'settings' => [
                    'number_of_shards' => 2,
                    'number_of_replicas' => 0
                ]
            ]
        ];

        $response = $this->client->indices()->create($params);

        return $response;
    }

    public function index_object($type, $id, $body = array()) {

        $params = [
            'index' => $this->index,
            'type' => $type,
            'id' => $id,
            'body' => $body
        ];

        $response = $this->client->index($params);

        return $response;
    }

    public function get_object($type, $id, $source=false) {

        $params = [
            'index' => $this->index,
            'type' => $type,
            'id' => $id
        ];

        if ($source) {
            $response = $this->client->getSource($params);
        }
        else {
            $response = $this->client->get($params);
        }

        return $response;
    }

    public function search_object($type, $param, $value) {

        $params = [
            'index' => $this->index,
            'type' => $type,
            'body' => [
                'query' => [
                    'match' => [
                        $param => $value
                    ]
                ]
            ]
        ];

        $response = $this->client->search($params);

        return $response['hits'];
    }

    public function delete_object($type, $id) {

        $params = [
            'index' => $this->index,
            'type' => $type,
            'id' => $id
        ];

        $response = $this->client->delete($params);

        return $response;
    }

    public function delete_index($index) {

        $deleteParams = [
            'index' => $index
        ];

        $this->client->indices()->delete($deleteParams);

        return true;
    }

    static public function create_default_index() {
        # creates elastic search index
        $settings = require __DIR__. '/../settings.php';
        static::create_index($settings['settings']['default_index']);
    }
}