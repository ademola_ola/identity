<?php
/**
 * Created by PhpStorm.
 * User: codesmata
 * Date: 1/17/17
 * Time: 2:02 PM
 */

namespace App\Services;


use App\Models\Contacts;
use App\Models\User;
use App\Models\Group;
use App\Models\GroupMember;

class ContactManagement
{

    /**
     * @param $contact_info
     * @return string
     */
    function createNewContact($contact_info){

        $contact_info=json_decode($contact_info);

        $isActive=false;
        $user_id=$_SESSION['user'];

        $user_info=User::find($user_id);

        if(count($user_info)<=0){
            return CodeUtility::returnMessage(405,'error','Invalid User','Please login to create this contact');
        }

        $user_phone=$user_info->phone;


        //confirm if this contact does not exist for this user
        $contact_check=Contacts::whereRaw('user_phone=? and (phone=? or email_address=?)',[$user_phone,
            $contact_info->phone,
            $contact_info->email])->get();

        if(count($contact_check)<=0){

            $contact_crt=Contacts::create([
                'user_phone'=>$user_phone,
                'first_name'=>$contact_info->firstname,
                'other_names'=>$contact_info->lastname,
                'full_name'=>$contact_info->firstname.' '.$contact_info->lastname,
                'email_address'=>$contact_info->email,
                'phone'=>$contact_info->phone,
                'image_url'=>'',
                'is_active'=>$isActive
            ]);

            if(!$contact_crt){
                return CodeUtility::returnMessage(405,'error','Error in connection','An error occurred while connecting to the database');
            }
        }

        //also save the user to a group if the group id is not 0
        if($contact_info->group_id!==0){
            if($this->keepUserInGroup($contact_crt->id,$contact_info->group_id,$user_id)!=='success'){
                CodeUtility::log_info("An Error occurred while adding this user to a group!!!");
            }
        }


        return CodeUtility::returnMessage(200,'success','Contact Creation Successful!','Contact Created Successfully!!');
    }


    function keepUserInGroup($contact_id,$group_id,$user_id){
        $group_info=Group::find($group_id);

        if(count($group_info)<=0){
            return CodeUtility::returnMessage(405,'error','Missing Group','Seens this group has been removed... Please reload the page');
        }

        $contact_check=GroupMember::whereRaw('group_id=? and contact_id=?',[$group_id,$contact_id])->get();

        if(count($contact_check)<=0){
            //create at this point, since it doesnt exist
            $member_crt=GroupMember::create([
                'user_id'=>$user_id,
                'group_id'=>$group_id,
                'contact_id'=>$contact_id
            ]);

            if(!$member_crt){
                CodeUtility::log_info("An error occurred while creating the group member");
            }
        }
        return 'success';
    }

}