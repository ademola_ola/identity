<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/26/16
 * Time: 12:37 AM
 */
namespace App\Services;
use App\Models\AuthorizationToken;
use App\Models\User;

class Token
{

    static public function generateToken($phone) {

        $match = ['phone' => $phone, 'is_expired' => false];

        $token = AuthorizationToken::where($match)->first();

        if ($token) {
            $_SESSION['phone'] = $phone;
            return $token->access_token;
        }

        $tokens = AuthorizationToken::select("access_token")->where('is_expired', false)->get();

        $accesses = array();

        foreach ($tokens as $t) {
            array_push($accesses, $t->access_token);
        }

//        $var = bin2hex(openssl_random_pseudo_bytes(2));
        $var = mt_rand(0,9999);

        while(in_array($var, $accesses)) {
//            $var = bin2hex(openssl_random_pseudo_bytes(2));
            $var = mt_rand(0,9999);
        }

        $token = AuthorizationToken::create([
            'phone' => $phone,
            'access_token' => $var,
            'is_expired' => false
        ]);

        $_SESSION['phone'] = $phone;

        return $token->access_token;
    }

    static public function expireToken($access_token) {
        $token = AuthorizationToken::where('access_token', $access_token)->first();

        if ($token) {
            $token->expire();
        }
    }

}