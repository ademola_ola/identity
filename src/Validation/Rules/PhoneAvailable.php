<?php

/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 11:38 PM
 */
namespace App\Validation\Rules;
use App\Models\User;
use Respect\Validation\Rules\AbstractRule;


class PhoneAvailable extends AbstractRule
{

    public function validate($input)
    {
        // TODO: Implement validate() method.
        return User::where('phone', $input)->count() === 0;
    }
}