<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 1:12 PM
 */

use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;

$app->get('/search', 'SearchController:getPage')->setName('search');
$app->get('/results', 'SearchProfileController:searchResult')->setName('results');


$app->group('', function (){

    $this->get('/', 'IndexController:index')->setName('index');

    $this->post('/', 'IndexController:postData');

    $this->get('/account/request', 'VerificationController:getPage')->setName('verify');

    $this->get('/account/confirm', 'ConfirmationController:getPage')->setName('confirm');

    $this->post('/account/confirm', 'ConfirmationController:postData');

    $this->post('/token/confirm', 'ConfirmationController:confirmToken');

    $this->post('/token/resend', 'ConfirmationController:resendToken');

    $this->post('/number/verify', 'ConfirmationController:verifyNumber');

    $this->get('/register', 'AuthController:getSignUp')->setName('register');

    $this->post('/register', 'AuthController:postSignUp');

    $this->get('/login', 'LoginController:getLogin')->setName('login');

    $this->post('/login', 'LoginController:postLogin');

    $this->get('/forgot', 'ForgotController:getPage')->setName('forgot');

    $this->post('/forgot', 'ForgotController:postPage');

    $this->get('/change', 'ChangeController:getPage')->setName('change');

    $this->post('/change', 'ChangeController:postPage');

})->add(new GuestMiddleware($container));

$app->group('', function (){

    // Account management routes

    $this->get('/password', 'PasswordController:getChangePassword')->setName('password');
    //=============user preferences
    $this->post('/password', 'PasswordController:postChangePassword');
    $this->get('/logout', 'AuthController:getLogOut')->setName('logout');

    $this->get('/settings', 'SettingsController:getPage')->setName('settings');
    $this->post('/basic_settings','SettingsController:createBasicSetting')->setName('basic_settings');


    //$this->post('/settings', 'SettingsController:postData');

    // Mailbox routes
    $this->get('/mailbox', 'MailController:getMail')->setName('mailbox');
    $this->post('/mailbox', 'MailController:sendMail');

    $this->get('/sent', 'MailController:getSent')->setName('sent');

    $this->get('/drafts', 'MailController:getDrafts')->setName('drafts');
    $this->post('/drafts', 'ComposeController:saveDraft');
    
    $this->get('/trash', 'MailController:getTrash')->setName('trash');
    $this->get('/spam', 'MailController:getSpam')->setName('spam');

    // =================== MESSAGE DETAIL
    $this->get('/{folder}/{message_id}/message', 'MailController:getDetail')->setName('detail');

    // =================== TRASH MESSAGE
    $this->post('/{folder}/{message_id}/trash', 'MailController:trashMessage')->setName('trash_message');

    // =================== REMOVE MESSAGE
    $this->post('/{folder}/{message_id}/delete', 'MailController:deleteMessage')->setName('delete_message');

    $this->post('/search/message', 'MailController:searchFolder')->setName('search_folder');

    // ================= COMPOSE MESSAGES
    $this->get('/compose', 'ComposeController:getPage')->setName('compose');
    $this->post('/compose', 'ComposeController:postData');
    $this->get('/compose/{recipient}', 'ComposeController:sendContact')->setName('compose_contact');

    $this->post('/upload', 'ComposeController:attachFiles');

    // =================== SYNC MAILBOX
    $this->get('/sync/mail', 'MailController:syncMail');
    $this->get('/sync/drafts', 'MailController:syncDrafts');
    $this->get('/sync/sent', 'MailController:syncSent');
    $this->get('/sync/trash', 'MailController:syncTrash');
    $this->get('/sync/spam', 'MailController:syncSpam');
    $this->post('/sync', 'MailController:syncMail');

    $this->post('/feed', 'LandingController:postFeed');
    $this->get('/feed', 'LandingController:getFeed')->setName('feed');
    $this->get('/news', 'LandingController:streamNews')->setName('news');
    
    // Instant Messaging routes
    $this->get('/chats', 'ChatController:getPage')->setName('chats');
    $this->post('/chats', 'ChatController:postData');

    // Google contacts
    $this->get('/contacts/google', 'ContactController:syncGoogle')->setName('google_contacts');
    $this->get('/contacts/google_feedback', 'ContactController:redirectRequest')->setName('google_feedback');

    // Google calendar
    $this->get('/google/calendar', 'CalendarController:syncGoogle')->setName('google_calendar');
    $this->get('/google/calendar_feedback', 'CalendarController:redirectRequest')->setName('calendar_feedback');

    $this->get('/contacts', 'ContactController:getPage')->setName('contacts');
    $this->post('/contact','ContactController:createContact')->setName('contact');
    $this->get('/contacts/feedback', 'ContactController:syncYahoo')->setName('yahoo_contacts');

    //=============================UPLOAD CONTACTS FOR A GROUP
    $this->post('/user/contact/groups','ContactController:keepContactGroup')->setName('keepContactGroups');
    $this->post('/uploadContact','ContactController:uploadContact')->setName('uploadContact');


    $this->get('/groups/{group_id}','ContactController:getGroup')->setName('view_group');
    $this->get('/groups','ContactController:getGroups')->setName('view_groups');

    //====================== LANDING PAGE
    $this->get('/home', 'LandingController:getPage')->setName('landing');
    $this->post('/home', 'LandingController:postData');

    // Email Account Configuration

    $this->get('/gmail', 'GmailController:readMail')->setName('gmail');
    $this->get('/gmail/feedback', 'GmailController:redirectRequest')->setName('feedback');

    $this->post('/email', 'EmailController:postData')->setName('email');
    $this->post('/yahoo', 'YahooController:postData')->setName('yahoo');
    $this->get('/custom', 'CustomController:requestAccess')->setName('custom');
    $this->get('/custom/feedback', 'YahooController:redirectUri')->setName('custom_feedback');

    $this->get('/notifications', 'NotificationController:getPage')->setName('notifications');

    $this->post('/sms', 'SmsController:postData')->setName('sms');

    $this->get('/notes', 'ForumController:getPage')->setName('notes');

    $this->get('/events', 'EventController:getPage')->setName('events');
    $this->post('/event','EventController:createEvent')->setName('event');

})->add(new AuthMiddleware($container));

$app->get('/profile', function ($request, $response){
    return $this->view->render($response, 'templates/admin/profile.twig');
})->setName('profile');

// api for mobile app
$app->group('/api/v1', function () {
   // contact api
    $this->get('/users/{user_id}/contacts', 'ApiController:getContacts')->setName('mobile_contacts');
    $this->get('/users/contact/platform', 'ApiController:getPlatformContacts')->setName('platform_contacts');


    $this->post('/contact','ContactController:createContact')->setName('contact');
    $this->post('/uploadContact','ContactController:uploadContact')->setName('uploadContact');


    $this->post('/group','ContactController:createGroup')->setName('groups');
    $this->get('/groups','ContactController:loadUserGroups')->setName('loadGroups');
    $this->get('/groups_contacts/{group_id}','ContactController:loadGroupContacts')->setName('loadGroupContacts');

    //=============================UPLOAD OUR
    $this->post('/user/contact/groups','ContactController:keepContactGroup')->setName('keepContactGroups');

    //=================EDUCATION SETTING
    $this->post('/education','SettingsController:createEducationSetting')->setName('education');
    $this->get('/education','SettingsController:loadEducationSettings')->setName('education');


    //============ work experience

    $this->post('/work','SettingsController:createWorkExperience')->setName('work');
    $this->get('/work_experience','SettingsController:loadWorkExperiences')->setName('work_experience');

    //======================personal mail preference settings
    $this->post('/preference','SettingsController:createUserPreference')->setName('preference');
    $this->get('/preference','SettingsController:loadUserPreference')->setName('get_preference');


});

$app->get('/{number}', 'SearchProfileController:getPage');
