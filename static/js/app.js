/**
 * Created by stikks on 8/7/16.
 */
'use strict';

var blobToFile=function(blob,lastModified,fileName){
    //blob.lastModified=lastModified;
    //blob.name=fileName;
    return new File([blob],fileName);
};

var displayToast = function (duration, message,action='success', redirect=false,redirect_url=null) {
    $(document).ready(function () {
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                positionClass:'toast-top-center',
                timeOut: duration
            };
            if(action == 'success'){
                toastr.success(message);
            }
            else if (action == 'info') {
                toastr.info(message);
            }
            else{
                toastr.error(message);
            }
           // setTimeout(function(){
           //     if(redirect && redirect_url){
           //        // location.href=redirect_url;
           //     }
           // },1000);
        }, duration);
    });
};

var _app = angular.module('mainApp', ['angularMoment', 'ngSanitize', "checklist-model"]);

_app.directive('ngFileModel', ['$parse', function ($parse) {
    /*return {
        scope:true,
        link: function(scope,el,attrs){
            el.bind('change',function(event){
               var files=event.target.files;

                for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    scope.$emit("fileSelected", { file: files[i] });
                    console.log("File Selected");
                }

            });
        }
    }*/

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;

            element.bind('change', function (event) {
                scope.message.error = null;
                angular.forEach(event.target.files, function (item) {

                    var value = {
                        name: item.name,            //filename
                        size: Number(item.size),        //filesize;
                        url: URL.createObjectURL(item), //file url
                        type:item.type,                     //file type
                        // File Input Value
                        _file: item
                    };

                    var converted = scope.uploaded_files.map(function (elem) {
                        return elem.size;
                    });

                    var total_size = converted.reduce(function(a, b) {
                        return Number(a) + Number(b);
                    }, 0);

                    if (value.size <= scope.size_limit && (total_size + value.size) <= scope.size_limit) {
                        scope.uploaded_files.push(value);
                        /*scope.$apply(function() {
                            scope.uploaded_files.push(value);
                            var x = scope.uploaded_files.map(function (elem) {
                                return new File([elem.url],elem.name)
                            });

                            // document.getElementById('file-upload').files = x[0];
                        });*/
                    }
                    else {
                        scope.$apply(function () {
                            scope.message.error = "You cannot upload files exceeding " + scope.size_limit / 1000000 + 'MB';
                        });
                    }
                });
            });
        }
    };
}]);

_app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol("[[").endSymbol("]]");
});

_app.controller('MainController', function ($scope, $timeout, $q) {

    var load_code = function () {
        $timeout(function () {
            // http://ip-api.com/json
            $.get("https://freegeoip.net/json/", function(response) {
                $scope.$apply(function () {
                    $scope.data.countryCode = response.country_name;
                });
                // if (response.status == 'success') {
                //     $scope.$apply(function () {
                //         $scope.data.countryCode = response.country_name;
                //     });
                // }
            }, "jsonp");
        }, 1);
    };

    var checkToken = function () {
        $.post('/token/confirm', {
            token: $scope.variable.token,
            phone: $scope.variable.phone
        }, function (data, status) {
            if (data.response == 'success') {
                $scope.$apply(function() {
                    $scope.variable.loading = false;
                    $scope.variable.valid = true;
                });
            }
            else {
                $scope.$apply(function() {
                    $scope.variable.loading = false;
                    $scope.variable.error = 'This token is not valid.'
                });
            }
        })
    };

    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $('#token').keyup(function() {
        $scope.$apply(function () {
            $scope.variable.loading = true;
        });
        delay(function(){
            // $scope.variable.token = document.getElementById('token').value;
            $scope.variable.error = null;
            $scope.variable.success = null;
            checkToken()
        }, 1200 );
    });

    // var verifyNumber = function () {
    //     $scope.$apply(function () {
    //         $scope.data.loading = true;
    //     });
    //     $.post('/number/verify', {phone: $scope.data.number}, function (data, status) {
    //         if (data.response == 'success') {
    //             $scope.$apply(function() {
    //                 $scope.data.loading = false;
    //                 $scope.data.valid = true
    //             });
    //         }
    //         else {
    //             $scope.$apply(function() {
    //                 $scope.data.loading = false;
    //                 $scope.data.error = 'This Mobile PhoneNumber is not valid.'
    //             });
    //         }
    //     })
    // };

    // $('#phone_number').keyup(function() {
    //     delay(function(){
    //         $scope.data.error = null;
    //         $scope.data.number = document.getElementById('countryCode').value + document.getElementById('phone_number').value;
    //         verifyNumber()
    //     }, 1500 );
    // });

    $scope.startParallel = function () {
        $q.all([load_code()]).then(
            function (successResult) {
            }, function (failureReason) {
            }
        );
    };

    $scope.init = function (countries) {
        $scope.data = {"countryCode": null,
            'countries': JSON.parse(countries),
            'loading': false,
            'number': null,
            'valid': true,
            'phone': null,
            'error': null
        };
        $scope.startParallel();
    };

    $scope.confirmation = function (phone) {
        $scope.variable = {
            'token': null,
            'password': null,
            'valid': false,
            'phone': phone,
            'error': null,
            'success': null
        };
    };

    $scope.resendToken = function() {
        $scope.variable.loading = true;
        $.post('/token/resend', {
            phone: $scope.variable.phone
        }, function (data, status) {
            if (data.response == 'success') {
                $scope.$apply(function() {
                    $scope.variable.loading = false;
                    $scope.variable.success = 'Token resent';
                });
            }
            else {
                $scope.$apply(function() {
                   location.href = '/';
                });
            }
        })
    }

});

_app.controller('HomeController', function ($scope, $timeout) {

    $scope.init = function () {
        $scope.res = {"success": null, "error": null, query: null };
        $scope.searching = false;
        // $scope.option = 'sms';
        // $scope.phone = null;
        // $scope.news_data = [];
        // $scope.events_feed = events;

        // $timeout(function () {
        //     $.get("https://newsapi.org/v1/articles?source=time&sortBy=top&apiKey=8aeff1d37aa94937a563799dee3cc3b7&size=8", function (data, status) {
        //         if (data.status == "ok") {
        //             $scope.$apply(function(){
        //                 $scope.news_data = data.articles.filter(function (elem) {
        //                     if (elem.urlToImage) {
        //                         return elem
        //                     }
        //                 });
        //                 $scope.news_data = $scope.news_data.splice(0,6);
        //             });
        //         }
        //     });
        // }, 100);
    };

    // $scope.message = {"success": null, "error": null};
    //
    // $scope.postMessage = function () {
    //     $scope.submitted = true;
    //     $.post('/feed', { "text": $scope.text }, function (res, err) {
    //         if (res == "success") {
    //             $scope.message.success = "Your message has been successfully posted.";
    //             location.href = "/home";
    //         }
    //         else {
    //             $scope.message.error = "Your message post was unsuccessful."
    //         }
    //     })
    // };

    $scope.searchFolder = function (folder) {
        $scope.searching = true;
        $.post('/search/message', {
            query: $scope.res.query
        }, function (data, status) {

        })
    }

});

_app.controller('ComposeController', function ($scope, $location, $timeout, $q, $http) {


    $scope.init = function (account, url) {
        $scope.loading = true;
        $scope.data = {
            "messages": [],
            "message_count": 0,
            "from": account,
            "to": [],
            "cc": [],
            "bcc": [],
            'subject': null,
            'clicked': false
        };
        $scope.data.message_count = 0;
        // startParallel();
        $scope.res = {success: "", error: null};
        $scope.files = [];
        $scope.message = {"error": null, "response": null};
        $scope.loading = false;
    };

    $scope.upload = function(){
        alert($scope.files.length+" files selected ... Write your Upload Code");
    };
    
    $scope.draft = function () {
        displayToast(0, 'Saving to Drafts','info');
        var _data = {
            message: $('.summernote').code(),
            from: $scope.data.from,
            to: document.getElementById('to').value,
            cc: document.getElementById('cc').value,
            bcc: document.getElementById('bcc').value,
            subject: $scope.data.subject
        };
        $.post("/drafts", _data, function (data, status) {
            toastr.clear();
            displayToast(100, 'Saved','success');
            $timeout(function () {
                location.href = '/drafts'
            }, 5);
        })
    };

    // $scope.draft = function (email) {
    //     displayToast(0, 'Saving to Drafts','info');
    //     var _data = {
    //         message: $('.summernote').code(),
    //         from: email,
    //         to: $scope.message.from.substring($scope.message.from.lastIndexOf("<")+1, $scope.message.from.lastIndexOf(">")),
    //         subject: 'Re: ' + $scope.message.subject,
    //         cc: '',
    //         bcc: ''
    //     };
    //     $.post("/drafts", _data, function (data, status) {
    //         toastr.clear();
    //         displayToast(100, 'Saved','success');
    //         $timeout(function () {
    //             location.href = '/drafts'
    //         }, 5);
    //     })
    // };

    var load_inbox = function () {
        $timeout(function () {
            $.get("/sync/mail", function (data, status) {
                var _data = JSON.parse(data);
                $scope.$apply(function () {
                    $scope.data.message_count = _data.message_count;
                    $scope.data.messages = _data.messages.reverse();
                });
            });
        }, 10);
    };

    var startParallel = function () {
        $q.all([load_inbox()]).then(
            function (successResult) {
                $scope.loading = false;
            }, function (failureReason) {
            }
        );
    };

    // var myDropzone = new Dropzone('#upload-files', {
    //     paramName: "files",
    //     maxFilesize: 3.0,
    //     maxFiles: 4,
    //     parallelUploads: 10000,
    //     uploadMultiple: true,
    //     autoProcessQueue: false
    // });

    $scope.send = function () {
        var output = $('.summernote').code();
        var output_files = $scope.uploaded_files.map(function (elem) {
         //   console.log(elem);
            return blobToFile(elem.url,elem.lastModified,elem.name);
        });

        var formInfo={
            message: output,
            from: $scope.data.from,
            to: $scope.to,
            cc: $scope.cc,
            bcc: $scope.bcc,
            subject: $scope.subject
        };

        $http({
            method:'POST',
            url:'/compose',
            headers:{'Content-Type':undefined},
            transformRequest: function(data){
                var formData=new FormData();
                formData.append("model",angular.toJson(formInfo));

                for(var i=0; i<=data.files; i++){
                    console.log(data.Files[i]);
                    formData.append("file"+i,data.Files[i]);
                }

                return formData;
            },
            data:{model:formInfo,files:output_files}
        }).success(function(data,status,header,config){
            alert(data);

        }).error(function(data,status,header,config){
            alert("Failed!!");
        });

    };

    $("#sendNewEmailMessage").ajaxForm(function (_data) {
        var data = JSON.parse(_data);
        if(data.status == 'success') {
            toastr.clear();
            displayToast(100, 'Message Sent','success');
            location.href = '/';
        }
        else {
            toastr.clear();
            $scope.$apply(function () {
                $scope.loading = false
            });
            displayToast(1500, 'Message Not Saved to your sent folder.','error');
        }
    });

});

_app.controller('MailboxController', function ($scope, $timeout, $q) {

    var load_inbox = function () {
        $timeout(function () {
            $.get("/sync/mail", function (data, status) {
                var _data = JSON.parse(data);
                $scope.$apply(function () {
                    $scope.data.message_count = _data.message_count;
                    $scope.data.messages = _data.messages.reverse();
                });
            });
            $scope.loading = false;
        }, 10);
    };

    var startParallel = function () {
        $q.all([load_inbox()]).then(
            function (successResult) {
            }, function (failureReason) {
            }
        );
    };

    $scope.init = function (message_count) {
        $scope.loading = true;
        $scope.data = {"messages": [], "message_count": 0, "roles": [], "checked": false};
        $scope.data.message_count = message_count;
        startParallel();
    };

    $scope.checkAll = function() {
        $scope.data.checked = true;
        $scope.data.roles = angular.copy($scope.data.messages);
        $scope.data.messages.forEach(function(elem) {
            elem.checked = true;
        })
    };
    $scope.uncheckAll = function() {
        $scope.data.checked = false;
        $scope.data.roles = [];
        $scope.data.messages.forEach(function(elem) {
            elem.checked = false;
        })
    };

    $scope.toggleSelection = function (role) {
        var message = $scope.data.messages.find(function (res) {
            return res == role;
        });

        if($scope.data.roles.indexOf(role) == -1){
            $scope.data.roles.push(role);
            message.checked = true;
        }
        else{
            $scope.data.roles.splice($scope.data.roles.indexOf(role),1);
            message.checked = false;
        }
    };

    $scope.init_drafts = function (message_count) {
        $scope.loading = true;
        $scope.data = {"messages": [], "message_count": 0};
        $scope.data.message_count = message_count;
        $timeout(function () {
            $.get("/sync/drafts", function (data, status) {
                var _data = JSON.parse(data);
                $scope.$apply(function () {
                    $scope.data.message_count = _data.message_count;
                    $scope.data.messages = _data.messages.reverse();
                });
            });
            $scope.loading = false;
        }, 5);
    };

    $scope.init_sent = function (message_count) {
        $scope.loading = true;
        $scope.data = {"messages": [], "message_count": 0};
        $scope.data.message_count = message_count;
        $timeout(function () {
            $.get("/sync/sent", function (data, status) {
                var _data = JSON.parse(data);
                $scope.$apply(function () {
                    $scope.data.message_count = _data.message_count;
                    $scope.data.messages = _data.messages.reverse();
                });
            });
            $scope.loading = false;
        }, 5);
    };

    $scope.init_trash = function (message_count) {
        $scope.loading = true;
        $scope.data = {"messages": [], "message_count": 0};
        $scope.data.message_count = message_count;
        $timeout(function () {
            $.get("/sync/trash", function (data, status) {
                var _data = JSON.parse(data);
                $scope.$apply(function () {
                    $scope.data.message_count = _data.message_count;
                    $scope.data.messages = _data.messages.reverse();
                });
            });
            $scope.loading = false;
        }, 5);
    };

    $scope.init_spam = function (message_count) {
        $scope.loading = true;
        $scope.data = {"messages": [], "message_count": 0};
        $scope.data.message_count = message_count;
        $timeout(function () {
            $.get("/sync/spam", function (data, status) {
                var _data = JSON.parse(data);
                $scope.$apply(function () {
                    $scope.data.message_count = _data.message_count;
                    $scope.data.messages = _data.messages.reverse();
                });
            });
            $scope.loading = false;
        }, 5);
    };
});

_app.controller('SentController', function ($scope, $timeout) {

    $scope.init = function (message_count) {

        $scope.data = {"messages": [], "message_count": 0};
        $scope.data.message_count = message_count;

        $timeout(function () {
            $.get("/sync/mail", function (data, status) {
                var _data = JSON.parse(data);
                $scope.data.message_count = _data.message_count;
                $scope.data.messages = _data.messages;
            });
        }, 10);

    };
});

_app.controller('MessageController', function ($scope, $timeout) {

    $scope.init = function (from, to, cc, reply_to, subject, body, number, folder, date) {
        $scope.loading = true;
        $scope.message = {'from': from, 'to': to, 'cc': cc, 'reply_to': reply_to, 'subject': subject, 'folder': folder, 'number': number, 'body': body, 'date': date};
        $scope.trash = {'clicked': false};
        $timeout(function () {
            $scope.loading = false
        }, 1000)
    };

    $scope.trashMessage = function (message_id, folder) {
        $scope.trash.clicked = true;
        var url = '/' + folder + '/' + message_id;
        if (folder == 'Trash') {
            displayToast(0, 'Deleting...','info');
            url = url + '/delete';
        }
        else {
            displayToast(0, 'Moving to Trash','info');
            url = url + '/trash';
        }
        $timeout(function () {
            $.post(url, {message_id: message_id, folder: folder}, function (data, status) {
                toastr.clear();
                displayToast(100, 'Successful...','success');
                location.href = '/mailbox';
            });
        }, 5)
    };

    $scope.draft = function (email) {
        displayToast(0, 'Saving to Drafts','info');
        var _data = {
            message: $('.summernote').code(),
            from: email,
            to: $scope.message.from.substring($scope.message.from.lastIndexOf("<")+1, $scope.message.from.lastIndexOf(">")),
            subject: 'Re: ' + $scope.message.subject,
            cc: '',
            bcc: ''
        };
        $.post("/drafts", _data, function (data, status) {
            toastr.clear();
            displayToast(100, 'Saved','success');
            $timeout(function () {
                location.href = '/drafts'
            }, 5);
        })
    };

    $scope.send = function (email) {
        displayToast(2000, 'Sending Message','info');
        var _data = {
            message: $('.summernote').code(),
            from: email,
            to: $scope.message.from.substring($scope.message.from.lastIndexOf("<")+1, $scope.message.from.lastIndexOf(">")),
            subject: 'Re: ' + $scope.message.subject,
            cc: '',
            bcc: ''
        };
        $scope.loading = true;
        $.post("/compose", _data, function (data, status) {
            toastr.clear();
            displayToast(100, 'Message Sent','success');
            $timeout(function () {
                location.href = '/'
            }, 5);
        })
    };
});

_app.controller('ContactController', function ($scope, $timeout) {

    $scope.init = function () {
        $scope.data = {"groups": []};
        $timeout(function () {
            $.get('/api/v1/groups', function (_data) {
                $scope.$apply(function () {
                    $scope.data.groups = JSON.parse(_data);
                })
                console.log($scope.data.groups);
            });
        }, 1)
    };

    $scope.recipient = null;
    $scope.changeRecipient = function (recipient) {
        $(".modal-body #to").val(recipient);
    };
    $scope.createGroup = function () {
        console.log('here');
        // $("#createNewContactModal").modal('hide');
        // $("#createNewGroupModal").modal("open");
    }
});

_app.controller('EventCtrl', function ($scope) {
   $scope.init = function (events) {
       var calendar_events = events.map(function (res) {
           return {
               title: res.name,
               start: new Date(res.start_date),
               end: new Date(res.end_date),
               url: res.link
           }
       });

       $('#calendar').fullCalendar({
           header: {
               left: 'prev,next today',
               center: 'title',
               right: 'month,agendaWeek,agendaDay'
           },
           editable: true,
           droppable: true, // this allows things to be dropped onto the calendar
           drop: function() {
               // is the "remove after drop" checkbox checked?
               if ($('#drop-remove').is(':checked')) {
                   // if so, remove the element from the "Draggable Events" list
                   $(this).remove();
               }
           },
           events: calendar_events
       });
   } 
});

_app.controller('SearchController', function($scope) {})