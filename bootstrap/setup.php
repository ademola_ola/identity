<?php
/**
 * Created by PhpStorm.
 * User: stikks
 * Date: 8/4/16
 * Time: 6:10 PM
 */
require 'app.php';

$container = $app->getContainer();
$db = $container->get('settings')['db'];
$pdo = new PDO('pgsql:dbname='.$db['database'].';host=localhost;user='.$db['username'].';password='.$db['password']);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

$list = array();
$add = "CREATE TABLE IF NOT EXISTS users (
    id serial,
	phone varchar (50) PRIMARY KEY,
	password varchar (255) NOT NULL,
	updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	created_at date
	);";

array_push($list, $add);

$details = "CREATE TABLE IF NOT EXISTS user_details(
  id serial PRIMARY KEY,
  password varchar(255) NOT NULL,
  plain_password varchar (255) NOT NULL,
  email varchar(100) NOT NULL UNIQUE,
  phone varchar REFERENCES users(phone),
  user_id INT NOT NULL,
  uid int NOT NULL,
  gid int NOT NULL,
  realname VARCHAR (255),
  first_name VARCHAR (255),
  last_name VARCHAR (255),
  address VARCHAR (255),
  country VARCHAR (255),
  website_url VARCHAR (255) NOT NULL,
  maildir VARCHAR (100) NOT NULL, 
  institution VARCHAR (255) NULL, 
  graduation_date DATE DEFAULT NULL, 
  company_name VARCHAR (255) NULL, 
  work_period DATE DEFAULT NULL,
  web_notification BOOL DEFAULT FALSE,  
  push_notification BOOL DEFAULT FALSE, 
  mobile_notification BOOL DEFAULT FALSE,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list, $details);

$accounts = "CREATE TABLE IF NOT EXISTS email_accounts(
  id serial,
  phone varchar (50) NOT NULL,
  access_token VARCHAR (255) NULL,
  mail_server VARCHAR (50) NOT NULL,
  email varchar(100) NOT NULL UNIQUE,
  default_sender BOOL DEFAULT FALSE,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list, $accounts);

$contacts = "CREATE TABLE IF NOT EXISTS contacts(
  id serial,
  user_phone VARCHAR(50) REFERENCES users(phone),
  first_name VARCHAR (255) NULL,
  other_names VARCHAR (50) NULL,
  full_name varchar(100) NULL,
  email_address VARCHAR (255) NULL,
  phone VARCHAR (255) NULL,
  image_url VARCHAR(355) NULL,
  is_active BOOL DEFAULT FALSE,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list, $contacts);

$alias = "CREATE TABLE IF NOT EXISTS aliases(
  id serial PRIMARY KEY,
  email varchar(100) NOT NULL UNIQUE,
  alias VARCHAR (255) NOT NULL, 
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list, $alias);

$transport = "CREATE TABLE IF NOT EXISTS transports(
  id serial PRIMARY KEY,
  domain varchar(100) NOT NULL,
  transport VARCHAR (255) NOT NULL, 
  gid int NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list, $transport);

$access = "CREATE TABLE IF NOT EXISTS authorization_tokens(
  id serial PRIMARY KEY,
  access_token VARCHAR NOT NULL,
  is_expired BOOL DEFAULT FALSE,
  phone varchar NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list, $access);

$feeds = "CREATE TABLE IF NOT EXISTS feeds(
  id serial,
  phone VARCHAR (255) NOT NULL,
  text VARCHAR (255) NOT NULL,
  creator VARCHAR (255) NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list, $feeds);

$messages = "CREATE TABLE IF NOT EXISTS messages(
  id serial,
  phone VARCHAR (255) NOT NULL,
  text VARCHAR (255) NOT NULL,
  recipient VARCHAR (255) NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list, $messages);

$credit = "CREATE TABLE IF NOT EXISTS credits(
  id serial,
  phone varchar (50) PRIMARY KEY REFERENCES users(phone),
  units INT DEFAULT 0,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list, $credit);

$events = "CREATE TABLE IF NOT EXISTS events(
  id serial,
  phone VARCHAR (255) NOT NULL,
  name VARCHAR (255) NOT NULL,
  uniqueid VARCHAR (255) NOT NULL,
  description TEXT NULL,
  start_date TIMESTAMP NOT NULL,
  end_date TIMESTAMP NULL,
  link TEXT NULL,
  location VARCHAR (255) NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list, $events);

$groups="CREATE TABLE IF NOT EXISTS groups(
    id serial,
    user_id INTEGER,
    group_name VARCHAR (255) NOT NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_at date DEFAULT NULL
);";

array_push($list,$groups);

$group_members="CREATE TABLE IF NOT EXISTS group_members(
  id serial,
  user_id INTEGER,
  group_id INTEGER,
  contact_id INTEGER,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_at date DEFAULT NULL
);";

array_push($list,$group_members);

//============CREATING SCHEMA FOR INSTITUTION
$institution="CREATE TABLE IF NOT EXISTS institutions(
  id serial,
  user_id INTEGER NOT NULL ,
  school_name VARCHAR (200) NOT NULL,
  course VARCHAR (200) NOT NULL,
  certificate VARCHAR (100) NOT NULL ,
  grad_year VARCHAR (50) NOT NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_at date DEFAULT NULL
);";

array_push($list,$institution);

$work_places="CREATE TABLE IF NOT EXISTS work_places(
  id serial,
  user_id INTEGER NOT NULL,
  workplace VARCHAR (250) NOT NULL,
  post_held VARCHAR (250)  NOT NULL,
  start_date DATE NOT NULL,
  end_date DATE  NOT NULL,
  job_desc TEXT NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_at date DEFAULT NULL
);";

array_push($list,$work_places);

$skills="CREATE TABLE IF NOT EXISTS skills(
  id serial,
  user_id INTEGER NOT NULL,
  skills TEXT NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_at date DEFAULT NULL
);";

array_push($list,$skills);

$interests="CREATE TABLE IF NOT EXISTS interests(
  id serial,
  user_id INTEGER NOT NULL,
  interests TEXT NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_at date DEFAULT NULL
);";

array_push($list,$interests);

$user_preferences="CREATE TABLE IF NOT EXISTS user_preferences(
id serial,
user_id INTEGER NOT NULL,
signature TEXT NOT NULL,
undo_timer INTEGER NOT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);";

array_push($list,$user_preferences);



foreach ($list as $data) {
    $pdo->exec($data);
}

$domains = "INSERT INTO transports
  (domain , gid, transport)
VALUES
  ('unodentity.com', 1000, 'virtual');";

$obj = $pdo->prepare($domains);
$obj->execute();

//$_sql = "CREATE TABLE IF NOT EXISTS virtual_domains (
//  id serial PRIMARY KEY,
//  name varchar(50) NOT NULL,
//  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
//  created_at date DEFAULT NULL
//) ;";
//
//array_push($list, $_sql);
//
//$qsql = "CREATE TABLE IF NOT EXISTS user_details (
//  id serial PRIMARY KEY,
//  domain_id int NOT NULL,
//  user_phone varchar (50) NOT NULL,
//  password varchar(106) NOT NULL,
//  email varchar(100) NOT NULL UNIQUE,
//  website_url VARCHAR (255) NOT NULL,
//  FOREIGN KEY (user_phone) REFERENCES users(phone),
//  FOREIGN key (domain_id) REFERENCES virtual_domains(id),
//  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
//  created_at DATE DEFAULT NULL
//)";
//
//array_push($list, $qsql);
//
//$rsql = "CREATE TABLE IF NOT EXISTS virtual_aliases (
//  id serial PRIMARY KEY,
//  domain_id int NOT NULL,
//  source varchar(100) NOT NULL,
//  destination varchar(100) NOT NULL,
//  FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE,
//  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
//  created_at DATE DEFAULT NULL
//);";
