<?php

require __DIR__ . '/bootstrap/app.php';

use App\Services\Index;
use App\Models\UserDetail;

$details = UserDetail::where('phone', '+17055784399')->first();
$es = new Index();

$es->index_object('user_detail', $details->phone, array('phone'=>$details->phone, 'email'=>$details->email, 'website_url'=>$details->website_url, 'realname'=>$details->realname, 'user_id'=>$details->user_id));
